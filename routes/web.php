<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MonthlyReportController;
use App\Http\Controllers\MACDirectoryController;
use App\Http\Controllers\MACMeetingController;
use App\Http\Controllers\LGUCommitmentReportController;
use App\Http\Controllers\ActivitiesController;
use App\Http\Controllers\CsvImportController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\HhProfileController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/mr', [MonthlyReportController::class, 'create']);

Route::post('/mr/period', [MonthlyReportController::class, 'generate']);

Route::get('/mr/period/{id}', [MonthlyReportController::class, 'show']);

Route::patch('/mr/period/{id}', [MonthlyReportController::class, 'archive']);

Route::put('/mr/hh_profile/{id}', [MonthlyReportController::class, 'update']);

Route::post('/mr/coverage', [MonthlyReportController::class, 'store']);

Route::post('/mr/profile', [HhProfileController::class, 'store']);

Route::get('/mr/profile/{id}', [HhProfileController::class, 'fetchHHMembers']);

Route::get('/mr/member/', [HhProfileController::class, 'fetchHHMemberSex']);

Route::put('/mr/profile/{id}', [HhProfileController::class, 'update']);

Route::patch('/mr/profile/{id}', [HhProfileController::class, 'archive']);

Route::post('/mr/mac', [MACDirectoryController::class, 'store']);

Route::put('/mr/mac/{id}', [MACDirectoryController::class, 'update']);

Route::patch('/mr/mac/{id}', [MACDirectoryController::class, 'archive']);

Route::post('/mr/meeting', [MACMeetingController::class, 'store']);

Route::put('/mr/meeting/{id}', [MACMeetingController::class, 'update']);

Route::patch('/mr/meeting/{id}', [MACMeetingController::class, 'archive']);

Route::post('/mr/lgucommitment', [LGUCommitmentReportController::class, 'store']);

Route::put('/mr/lgucommitment/{id}', [LGUCommitmentReportController::class, 'update']);

Route::patch('/mr/lgucommitment/{id}', [LGUCommitmentReportController::class, 'archive']);

/*Route::get('mr/activities', [ActivitiesController::class, 'store']);*/

Route::post('/mr/activities', [ActivitiesController::class, 'store']);

Route::put('/mr/activities/{id}', [ActivitiesController::class, 'update']);

Route::patch('/mr/activities/{id}', [ActivitiesController::class, 'archive']);

Route::get('/import', [CsvImportController::class, 'showForm']);

Route::post('/import', [CsvImportController::class, 'hh_roster']);

Route::get('/report', [PdfController::class, 'showForm']);

Route::get('/generateMR/{muncity}', [PdfController::class, 'generateMR']);

Route::get('/test', [TestController::class, 'testPath']);

Route::get('/export', [ExcelController::class, 'showForm']);

Route::get('/generateHHCoverage/{province}/{muncity}/{from}/{to}', [ExcelController::class, 'generateHHCoverage']);

Route::get('/generateHHProfile/{province}/{muncity}/{from}/{to}', [ExcelController::class, 'generateHHProfile']);