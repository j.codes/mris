<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\LGUCommitmentReport;

class LGUCommitmentReportController extends Controller
{
    public function store(Request $request){
        if(Auth::user()){

            $lgucommitment = new LGUCommitmentReport;
            
            $lgucommitment->mr_id = $request->input('id');
            $lgucommitment->month = $request->input('month');
            $lgucommitment->year = $request->input('year');
            $lgucommitment->province_id = $request->input('province_id');
            $lgucommitment->mun_city_id = $request->input('mun_city_id');
            $lgucommitment->barangay = $request->input('barangay');
            $lgucommitment->ssa_gap = $request->input('ssa_gap');
            $lgucommitment->commitment = $request->input('commitment');
            $lgucommitment->timeline = $request->input('timeline');
            $lgucommitment->budget_source = $request->input('budget_source');
            $lgucommitment->status = $request->input('status');
            $lgucommitment->remarks = $request->input('remarks');
            $lgucommitment->user_id = (Auth::user()->id);
            $lgucommitment->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $lgucommitment = LGUCommitmentReport::find($id);

        if(Auth::user()->id == $lgucommitment->user_id){
            $lgucommitment->barangay = $request->input('barangay');
            $lgucommitment->ssa_gap = $request->input('ssa_gap');
            $lgucommitment->commitment = $request->input('commitment');
            $lgucommitment->timeline = $request->input('timeline');
            $lgucommitment->budget_source = $request->input('budget_source');
            $lgucommitment->status = $request->input('status');
            $lgucommitment->remarks = $request->input('remarks');
            $lgucommitment->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function archive($id){
        $lgucommitment = LGUCommitmentReport::find($id);

        if(Auth::user()->id == $lgucommitment->user_id){
            $lgucommitment->isActive = 0;
            $lgucommitment->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }
}
