<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function testPath()
    {
        $path = public_path('storage/images/dswd-car.png');
        dd($path); // Check if the file exists
    }
}
