<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\MonthlyReport;
use App\Models\MACDirectory;
use App\Models\MACMeeting;
use App\Models\LGUCommitmentReport;
use App\Models\Activities;
use App\Models\MunicipalCity;
use App\Models\HHProfile;
use App\Models\CsvData;
use App\Models\HHCoverage;

class MonthlyReportController extends Controller
{
    public function create(){
        if(Auth::user()){

            // For HH Profile
           $monthlyreports = MonthlyReport::where('user_id', Auth::user()->id)->get();
           $monthlyreport = MonthlyReport::where('isActive', 1)->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
           $lastmrid = MonthlyReport::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();
           $provincesAndMuncities = MunicipalCity::select('province', 'muncity')->orderBy('province', 'ASC')->get()->groupBy('province');

           // For HH Coverage
           $hhroster = NULL;

           // For HH Profile
           $hhprofiles = HHProfile::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           $lasthhprofileid = HHProfile::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For MAC Directory
           $macdirectories = MACDirectory::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           $lastmacid = MACDirectory::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For MAC Meetings
           $macmeetings = MACMeeting::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           $lastmacmeetingid = MACMeeting::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For LGU Commitment
           $lgucommitments = LGUCommitmentReport::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           $lastlgucommitmentsid = LGUCommitmentReport::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For Activities
           $activities = Activities::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           $lastactivityid = Activities::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

            return view('mr.create')->with("monthlyreports", $monthlyreports)->with("monthlyreport", $monthlyreport)->with("lastmrid", $lastmrid)->with("hhroster", $hhroster)->with("hhprofiles", $hhprofiles)->with("lasthhprofileid", $lasthhprofileid)->with("macdirectories", $macdirectories)->with("lastmacid", $lastmacid)->with("macmeetings", $macmeetings)->with("lastmacmeetingid", $lastmacmeetingid)->with("lgucommitments", $lgucommitments)->with("lastlgucommitmentsid", $lastlgucommitmentsid)->with("activities", $activities)->with("lastactivityid", $lastactivityid)->with("provincesAndMuncities", $provincesAndMuncities);  
        }
        else{
            return redirect('/login');
        }       
    }

    public function generate(Request $request){
        if(Auth::user()){
            $month = $request->input('date-month');
            $endMonth = date('m', strtotime($month));
            $endYear = date('Y', strtotime($month));

            $endDate = date('Y-m-d', strtotime($endYear."-".$endMonth."-15"));

            $tempDate = date('Y-m-d', strtotime($endDate. ' -1 months'));
            $startMonth = date('m', strtotime($tempDate));
            $startYear = date('Y', strtotime($tempDate));

            $startDate = date('Y-m-d', strtotime($startYear."-".$startMonth."-16"));

            $mr = new MonthlyReport;

            $mr->from = $startDate;
            $mr->to = $endDate;
            $mr->month = $endMonth;
            $mr->year = $endYear;
            $mr->province_id = $request->input('mr-province');
            $mr->mun_city_id = $request->input('mr-muncity');
            $mr->user_id = (Auth::user()->id);
            $mr->save();

            return redirect('/mr');
        }
        else{
            return redirect('/login');
        }
    }

    public function show($id){

        if(Auth::user()){

           // For HH Profile
           $selectedmr = MonthlyReport::find($id);
           /*$monthlyreports = MonthlyReport::where('user_id', Auth::user()->id)->get();
           $monthlyreport = MonthlyReport::where('isActive', 1)->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
           $lastmrid = MonthlyReport::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();
           $provincesAndMuncities = MunicipalCity::select('province', 'muncity')->orderBy('province', 'ASC')->get()->groupBy('province');*/

           //For MAC Directory
           //$macdirectories = MACDirectory::where('isActive', 1)->where('user_id', Auth::user()->id)->get();
           //$lastmacid = MACDirectory::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For HH Coverage
           $hhroster = CsvData::select('barangay')
               ->selectRaw('COUNT(CASE WHEN client_status IN (1, 19, 24) THEN 1 WHEN client_status IS NULL THEN 0 ELSE NULL END) as hh_count')
               ->selectRaw('COUNT(CASE WHEN member_status IN (1, 19, 24) THEN 1 WHEN member_status IS NULL THEN 0 ELSE NULL END) as entry_count')
               ->where('muncity', 'LIKE', $selectedmr->mun_city_id . '%')
               ->groupBy('barangay')
               ->get();

            /*$hhcoverage = CsvData::select('muncity')
                ->selectRaw('SUM(CASE WHEN client_status LIKE "1 - %" THEN 1 ELSE 0 END) as count_1')
                ->selectRaw('SUM(CASE WHEN client_status LIKE "19 %" THEN 1 ELSE 0 END) as count_19')
                ->selectRaw('SUM(CASE WHEN client_status LIKE "24 %" THEN 1 ELSE 0 END) as count_24')
                ->selectRaw('SUM(CASE WHEN client_status LIKE "1 - %" OR client_status LIKE "19%" OR client_status LIKE "24%" THEN 1 ELSE 0 END) as total_count')
                ->where('muncity', 'LIKE', $selectedmr->mun_city_id . '%')
                ->groupBy('muncity')
                ->first();*/

            $hhcoverage = CsvData::select('muncity')
                ->selectRaw('SUM(client_status = 1) as count_1')
                ->selectRaw('SUM(client_status = 19) as count_19')
                ->selectRaw('SUM(client_status = 24) as count_24')
                ->selectRaw('SUM(client_status IN (1, 19, 24)) as total_count')
                ->where('muncity', 'LIKE', $selectedmr->mun_city_id . '%')
                ->groupBy('muncity')
                ->first();

           // For MAC Meetings
           $macmeetings = MACMeeting::where('isActive', 1)->where('user_id', Auth::user()->id)->where('mr_id', $id)->get();
           $lastmacmeetingid = MACMeeting::orderBy('id', 'DESC')->pluck('id')->first();

           // For LGU Commitment
           $lgucommitments = LGUCommitmentReport::where('isActive', 1)->where('user_id', Auth::user()->id)->where('mr_id', $id)->get();
           $lastlgucommitmentsid = LGUCommitmentReport::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           // For Activities
           $activities = Activities::where('isActive', 1)->where('user_id', Auth::user()->id)->where('mr_id', $id)->get();
           $lastactivityid = Activities::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->pluck('id')->first();

           //return response()->json(['selectedmr' => $selectedmr]);
           
           return response()->json([
               'id' => $selectedmr->id,
               'month' => $selectedmr->month,
               'year' => $selectedmr->year,
               'province_id' => $selectedmr->province_id,
               'mun_city_id' => $selectedmr->mun_city_id,
               'hhroster' => $hhroster,
               'hhcoverage' => $hhcoverage,
               'macmeetings' => $macmeetings,
               'lastmacmeetingid' => $lastmacmeetingid,
               'lgucommitments' => $lgucommitments,
               'lastlgucommitmentsid' => $lastlgucommitmentsid,
               'activities' => $activities,
               'lastactivityid' => $lastactivityid
           ]);

        }
        else{
            return redirect('/login');
        } 
    }

    public function update(Request $request, $id){
        $mr = MonthlyReport::find($id);

        if(Auth::user()->id == $mr->user_id){
            $mr->pwd_male = $request->input('pwd-male');
            $mr->pwd_female = $request->input('pwd-female');
            $mr->fff_male = $request->input('fff-male');
            $mr->fff_female = $request->input('fff-female');
            $mr->sp_male = $request->input('sp-male');
            $mr->sp_female = $request->input('sp-female');
            $mr->save();

            return redirect('/mr');
        }
        else{
            return redirect('/login');
        }
    }

    public function store(Request $request){
        if (Auth::user()) {
            $mrId = $request->input('mrid');
            $month = $request->input('month');
            $year = $request->input('year');

            // Check if a record with the same mr_id, month, and year exists
            $existingRecord = HHCoverage::where('mr_id', $mrId)
                ->where('month', $month)
                ->where('year', $year)
                ->first();

            if ($existingRecord) {
                // Update the existing record
                $existingRecord->province_id = $request->input('province_id');
                $existingRecord->mun_city_id = $request->input('mun_city_id');
                $existingRecord->c_1 = $request->input('c1');
                $existingRecord->c_19 = $request->input('c19');
                $existingRecord->c_24 = $request->input('c24');
                $existingRecord->total = $request->input('tcount');
                $existingRecord->user_id = Auth::user()->id;
                $existingRecord->save();
            } else {
                // Create a new record
                $roster = new HHCoverage;
                $roster->mr_id = $mrId;
                $roster->month = $month;
                $roster->year = $year;
                $roster->province_id = $request->input('province_id');
                $roster->mun_city_id = $request->input('mun_city_id');
                $roster->c_1 = $request->input('c1');
                $roster->c_19 = $request->input('c19');
                $roster->c_24 = $request->input('c24');
                $roster->total = $request->input('tcount');
                $roster->user_id = Auth::user()->id;
                $roster->save();
            }

            return redirect('/mr');
            
        } else {
            return redirect('/login');
        }
    }

    public function archive($id){
        $mr = MonthlyReport::find($id);

        if(Auth::user()->id == $mr->user_id){
            $mr->isActive = 0;
            $mr->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

}
