<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Illuminate\Support\Collection;

use App\Models\MonthlyReport;
use App\Models\MACDirectory;
use App\Models\MACMeeting;
use App\Models\LGUCommitmentReport;
use App\Models\Activities;
use App\Models\MunicipalCity;

class ExcelController extends Controller
{
    public function showForm()
    {
        if(Auth::user()){
            
           $provincesAndMuncities = MunicipalCity::select('province', 'muncity')->orderBy('province', 'ASC')->get()->groupBy('province');

            return view('mr/export')->with("provincesAndMuncities", $provincesAndMuncities);
        }
        else{
            return redirect('/login');
        } 
    }

    public function generateHHCoverage(Request $request, $selectedProvince, $selectedMuncity, $selectedMonth, $selectedMonthTo)
    {
        $userId = Auth::id();

        $monthsData = [];
        $selectedMonthFrom = $selectedMonth;

        $groupedData = [];

        // Retrieve all unique mun_city_id values
        $uniqueMunCityIds = DB::table('h_h_roster_stat')
            ->select('mun_city_id')
            ->distinct()
            ->pluck('mun_city_id');

        foreach ($uniqueMunCityIds as $munCityId) {
            $groupedData[$munCityId] = [
                'mun_city_id' => $munCityId,
            ];

            $selectedMonthFrom = $selectedMonth;

            while ($selectedMonthFrom <= $selectedMonthTo) {
                $query = DB::table('h_h_roster_stat')
                    ->select('c_1', 'c_19', 'c_24', 'total')
                    ->where('province_id', $selectedProvince)
                    ->where('mun_city_id', $munCityId)
                    ->whereRaw("CONCAT(year, '-', month) = ?", [$selectedMonthFrom])
                    ->first(); // Use first() to retrieve a single record

                // Check if there's a record for the current mun_city_id and month
                if ($query) {
                    $groupedData[$munCityId][] = $query->c_1;
                    $groupedData[$munCityId][] = $query->c_19;
                    $groupedData[$munCityId][] = $query->c_24;
                    $groupedData[$munCityId][] = $query->total;
                } else {
                    // If no record, add zeros
                    $groupedData[$munCityId][] = 0;
                    $groupedData[$munCityId][] = 0;
                    $groupedData[$munCityId][] = 0;
                    $groupedData[$munCityId][] = 0;
                }

                // Move to the next month
                list($year, $month) = explode('-', $selectedMonthFrom);
                $month = (int)$month;
                $year = (int)$year;

                if ($month == 12) {
                    $month = 1;
                    $year++;
                } else {
                    $month++;
                }

                $selectedMonthFrom = sprintf('%04d-%02d', $year, $month);
            }
        }

        // Convert the grouped data into a collection
        $exportData = collect(array_values($groupedData));

        // Convert the collection to an array
        $exportDataArray = $exportData->toArray();

        return Excel::download(new class($exportDataArray, $selectedMonth, $selectedMonthTo) implements FromArray, WithHeadings, WithStyles {
            use Exportable;

            public function registerEvents(): array
                {
                    dd('eme 0');
                    return [
                        AfterSheet::class => function (AfterSheet $event) {
                            // Apply custom styling or formatting here
                            // For example, set font styles or cell background colors
                            $sheet = $event->sheet;
                            $sheet->getStyle('A1')->getFont()->setBold(true);
                            $sheet->getStyle('B2')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                        },
                    ];
                }

            private $data;
            private $selectedMonth;
            private $selectedMonthTo;

            public function __construct(array $data, $selectedMonth, $selectedMonthTo)
            {
                /*dd($data);*/

                $this->data = $data;
                $this->selectedMonth = $selectedMonth;
                $this->selectedMonthTo = $selectedMonthTo;
            }

            public function array(): array
            {
                return $this->data;
            }

            public function headings(): array
            {
                $headers = [];

                // Calculate the number of columns based on the data
                $columnCount = max(array_map('count', $this->data));

                if ($columnCount > 5) {
                    // If there are more than 5 columns, repeat the headers
                    $repeatedHeaders = [
                        'Code 1',
                        'Code 19',
                        'Code 24',
                        'Total',
                    ];

                    // Determine how many times to repeat the headers
                    $repeatCount = ceil(($columnCount - 5) / 4);

                    // Create the first row of headers and repeat it
                    $firstRow = [
                        ' ',
                        $this->selectedMonth,
                        ' ',
                        ' ',
                    ];

                    for ($i = 0; $i < $repeatCount; $i++) {
                        $nextMonth = date('Y-m', strtotime($this->selectedMonth . '-01 +1 month'));
                        $firstRow = array_merge($firstRow, [
                            ' ',
                            $nextMonth,
                            ' ',
                            ' ',
                        ]);
                        $this->selectedMonth = $nextMonth; // Update selectedMonth to the next month
                    }

                    $headers[] = $firstRow;

                    // Create the second row of headers
                    $secondRow = ['City \ Municipality'];

                    for ($i = 0; $i <= $repeatCount; $i++) {
                        $secondRow = array_merge($secondRow, $repeatedHeaders);
                    }

                    $headers[] = $secondRow;
                } else {
                    // If there are 5 or fewer columns, use the original headers
                    $headers = [
                        [
                            ' ',
                            $this->selectedMonth,
                            ' ', // Empty cell
                            ' ', // Empty cell
                            ' ', // Empty cell
                        ],
                        [
                            'City \ Municipality', // Second row of headings
                            'Code 1',
                            'Code 19',
                            'Code 24',
                            'Total',
                        ],
                    ];
                }

                return $headers;
            }

            public function styles(Worksheet $sheet)
            {
                // Set bold font for the first two rows (e.g., headings)
                $sheet->getStyle('1:2')->getFont()->setBold(true);

                // Find the coordinates of the last occupied cell
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                // Calculate the coordinate for the last cell
                $lastCellCoordinate = $highestColumn . $highestRow;

                // Apply alignment and borders to all cells within the range from A1 to the last cell
                $range = 'A1:' . $lastCellCoordinate;

                $sheet->getStyle($range)->applyFromArray([
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                        ],
                    ],
                ]);

                // Auto-size the first column based on content
                $sheet->getColumnDimension('A')->setAutoSize(true);

                // Group and merge every 4 cells starting from B1
                $startColumnIndex = 2; // Column B
                $endColumnIndex = $startColumnIndex + 3; // Initial range size (B to E)

                while ($endColumnIndex <= \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn)) {
                    $startColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startColumnIndex);
                    $endColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($endColumnIndex);

                    $sheet->mergeCells($startColumnLetter . '1:' . $endColumnLetter . '1');

                    $startColumnIndex += 4;
                    $endColumnIndex += 4;
                }
            }

        }, 'hh_coverage.xlsx');
    }

    public function generateHHProfile(Request $request, $selectedProvince, $selectedMuncity, $selectedMonth, $selectedMonthTo)
    {
        $userId = Auth::id();

        $monthsData = [];
        $selectedMonthFrom = $selectedMonth;
        $groupedData = [];

        // Define the profile types to fetch
        $profileTypes = ['PWD', 'Solo Parent', 'Farmer, FW, FF'];

        foreach ($profileTypes as $profileType) {
            // Retrieve all unique mun_city_id values for the current profile type
            $uniqueMunCityIds = DB::table('h_h_profile')
                ->select('mun_city_id')
                ->distinct()
                ->orderBy('mun_city_id')
                ->pluck('mun_city_id');

            foreach ($uniqueMunCityIds as $munCityId) {
                $groupedData[$profileType][$munCityId] = [
                    'mun_city_id' => $munCityId,
                ];

                $selectedMonthFrom = $selectedMonth;

                while ($selectedMonthFrom <= $selectedMonthTo) {
                    $query = DB::table('h_h_profile')
                        ->select([
                            'mun_city_id',
                            DB::raw('SUM(CASE WHEN sex = "MALE" THEN 1 ELSE 0 END) as male_count'),
                            DB::raw('SUM(CASE WHEN sex = "FEMALE" THEN 1 ELSE 0 END) as female_count'),
                            DB::raw('COUNT(*) as total_count'),
                            DB::raw('CONCAT(year, "-", LPAD(month, 2, "0")) as formatted_month_year'),
                            DB::raw('DATE_FORMAT(updated_at, "%Y-%m") as formatted_updated_at'),
                            'isActive'
                        ])
                        ->where('profile', $profileType) // Use the current profile type
                        ->where('province_id', $selectedProvince)
                        ->where('mun_city_id', $munCityId)
                        ->groupBy('mun_city_id', 'year', 'month', 'updated_at', 'isActive')
                        ->first();

                    if ($query) {

                        $currentMonth = $query->formatted_month_year;
                        $updateDate = $query->formatted_updated_at;

                        if ($query->isActive == 1) {
                            if ($selectedMonthFrom >= $currentMonth && $currentMonth <= $selectedMonthTo) {
                                $groupedData[$profileType][$munCityId][] = $query->male_count;
                                $groupedData[$profileType][$munCityId][] = $query->female_count;
                                $groupedData[$profileType][$munCityId][] = $query->total_count;
                            } else {
                                $groupedData[$profileType][$munCityId][] = 0;
                                $groupedData[$profileType][$munCityId][] = 0;
                                $groupedData[$profileType][$munCityId][] = 0;
                            }
                        } else {
                            if ($selectedMonthFrom >= $currentMonth && $currentMonth <= $selectedMonthTo && $selectedMonthFrom <= $updateDate) {
                                $groupedData[$profileType][$munCityId][] = $query->male_count;
                                $groupedData[$profileType][$munCityId][] = $query->female_count;
                                $groupedData[$profileType][$munCityId][] = $query->total_count;
                            } else {
                                $groupedData[$profileType][$munCityId][] = 0;
                                $groupedData[$profileType][$munCityId][] = 0;
                                $groupedData[$profileType][$munCityId][] = 0;
                            }
                        }
                    } else {
                        $groupedData[$profileType][$munCityId][] = 0;
                        $groupedData[$profileType][$munCityId][] = 0;
                        $groupedData[$profileType][$munCityId][] = 0;
                    }

                    list($year, $month) = explode('-', $selectedMonthFrom);
                    $month = (int)$month;
                    $year = (int)$year;

                    if ($month == 12) {
                        $month = 1;
                        $year++;
                    } else {
                        $month++;
                    }

                    $selectedMonthFrom = sprintf('%04d-%02d', $year, $month);
                }
            }
        }

        // Convert the grouped data into a collection
        $exportData = collect(array_values($groupedData));

        // Convert the collection to an array
        $exportDataArray = $exportData->toArray();

        return Excel::download(new class($exportDataArray, $selectedMonth, $selectedMonthTo) implements FromArray, WithHeadings, WithStyles, WithEvents {
            use Exportable;

            private $data;
            private $selectedMonth;
            private $selectedMonthTo;

            public function __construct(array $data, $selectedMonth, $selectedMonthTo)
            {
                /*dd($data);*/

                $this->data = $data;
                $this->selectedMonth = $selectedMonth;
                $this->selectedMonthTo = $selectedMonthTo;
            }

            public function array(): array
            {
                $tables = [];

                $headerText = [
                    'HHs with Person/s with Disability (PWD)',
                    'HHs with Solo Parent',
                    'Head engaged as farmer/forest worker/fisher folks',
                ];

                // Iterate over each index of the data array
                foreach ($this->data as $index => $tableData) {
                    // Get the header text for the current table
                    $headerTextForIndex = $headerText[$index];

                    // Add headers for the current table
                    $table = $this->getHeadersForIndex($index, $headerTextForIndex);

                    // Construct the table array for the current index
                    foreach ($tableData as $cityName => $rowData) {
                        $table[] = $rowData; // Add city data
                    }

                    $tables[] = $table;
                }

                return $tables;
            }

            public function headings(): array
            {
                // The headings will be generated within the `getHeadersForIndex` function.
                return [];
            }

            public function getHeadersForIndex($index, $headerText): array
            {
                $headers = [];

                $columnCount = max(array_map('count', $this->data[$index]));

                // Store the original value of $this->selectedMonth
                $originalSelectedMonth = $this->selectedMonth;

                if ($columnCount > 4) {
                    $headers[] = [$headerText];

                    $repeatedHeaders = [
                        'Male',
                        'Female',
                        'Total',
                    ];

                    $repeatCount = ceil(($columnCount - 4) / 3);

                    $firstRow = [
                        ' ',
                        $this->selectedMonth,
                        ' ',
                        ' ',
                    ];

                    for ($i = 0; $i < $repeatCount; $i++) {
                        $nextMonth = date('Y-m', strtotime($this->selectedMonth . '-01 +1 month'));
                        $firstRow = array_merge($firstRow, [
                            $nextMonth,
                            ' ',
                            ' ',
                        ]);
                        
                        $this->selectedMonth = $nextMonth;
                    }

                    $headers[] = $firstRow;

                    // Restore the original value of $this->selectedMonth
                    $this->selectedMonth = $originalSelectedMonth;

                    // Create the second row of headers for each table
                    $headerRow = ['City \ Municipality'];

                    for ($j = 0; $j <= $repeatCount; $j++) {
                        $headerRow = array_merge($headerRow, $repeatedHeaders);
                    }

                    $headers[] = $headerRow;
                } else {
                    $headers[] = [$headerText];

                    $headers[] = [
                        ' ',
                        $this->selectedMonth,
                        ' ',
                        ' ',
                    ];

                    // Create the second row of headers for each table
                    $headerRow = ['City \ Municipality', 'Male', 'Female', 'Total'];

                    $headers[] = $headerRow;
                }

                return $headers;
            }

            public function styles(Worksheet $sheet)
            {
                // Find the coordinates of the last occupied cell
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                // Calculate the coordinate for the last cell
                $lastCellCoordinate = $highestColumn . $highestRow;

                // Apply alignment and borders to all cells within the range from A1 to the last cell
                $range = 'A1:' . $lastCellCoordinate;

                $sheet->getStyle($range)->applyFromArray([
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                        ],
                    ],
                ]);

                // Auto-size the first column based on content
                $sheet->getColumnDimension('A')->setAutoSize(true);

                // Get the column index for the highestColumn
                $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

                // Group and merge every 4 cells starting from B2
                $startColumnIndex = 2; // Column B
                $endColumnIndex = $startColumnIndex + 2; // Initial range size (B to E)

                while ($endColumnIndex <= $highestColumnIndex) {
                    $startColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startColumnIndex);
                    $endColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($endColumnIndex);

                    $sheet->mergeCells($startColumnLetter . '2:' . $endColumnLetter . '2');

                    $startColumnIndex += 3;
                    $endColumnIndex += 3;
                }
            }

            public function registerEvents(): array
            {
                return [
                    AfterSheet::class => function (AfterSheet $event) {
                        $sheet = $event->sheet;

                        // Define the array of text to match
                        $headerText = [
                            'HHs with Person/s with Disability (PWD)',
                            'HHs with Solo Parent',
                            'Head engaged as farmer/forest worker/fisher folks',
                        ];

                        // Get the highest row and column in the sheet
                        $highestRow = $sheet->getHighestRow();
                        $highestColumn = $sheet->getHighestColumn();
                        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

                        // Iterate through all cells from A1 to the highest column and row
                        for ($row = 1; $row <= $highestRow; $row++) {
                            for ($col = 'A'; $col <= $highestColumn; $col++) {
                                $cell = $sheet->getCell($col . $row);

                                // Check if the content of the cell matches any of the text in $headerText
                                if (in_array($cell->getValue(), $headerText)) {
                                    // Apply styling to the cell
                                    $cell->getStyle()->applyFromArray([
                                    'font' => ['bold' => true],
                                    ]);

                                    // Merge the entire row from A to the highestColumn
                                    $sheet->mergeCells("A$row:$highestColumn$row");

                                    // Identify the row next to the cell that matches any of the text in $headerText
                                    $nextRow = $row + 1;

                                    // Merge every 3 cells from B to the $highestColumn
                                    $startColumnIndex = 2; // Column B
                                    $endColumnIndex = $startColumnIndex + 2; // Initial range size (B to E)

                                    while ($endColumnIndex <= $highestColumnIndex) {
                                        $startColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startColumnIndex);
                                        $endColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($endColumnIndex);

                                        $sheet->mergeCells($startColumnLetter . $nextRow . ':' . $endColumnLetter . $nextRow);

                                        $startColumnIndex += 3;
                                        $endColumnIndex += 3;
                                    }
                                }
                            }
                        }
                    },
                ];
            }







        }, 'hh_profile.xlsx');
    }

}