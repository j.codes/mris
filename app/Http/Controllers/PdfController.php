<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PDF;
use Dompdf\Dompdf;

use App\Models\MonthlyReport;
use App\Models\MACDirectory;
use App\Models\MACMeeting;
use App\Models\LGUCommitmentReport;
use App\Models\Activities;
use App\Models\MunicipalCity;
use App\Models\CsvData;

require '../vendor/autoload.php';

class PdfController extends Controller
{
    public function showForm()
    {
        if(Auth::user()){
            
           $provincesAndMuncities = MunicipalCity::select('province', 'muncity')->orderBy('province', 'ASC')->get()->groupBy('province');

            return view('mr/report')->with("provincesAndMuncities", $provincesAndMuncities);
        }
        else{
            return redirect('/login');
        } 
    }

    public function generateMR(Request $request, $selectedMuncity)
       {
           $userId = Auth::id(); 

           $selectedMuncity = $request->input('muncity');

           $month = $request->input('month');
           $endMonth = date('m', strtotime($month));
           $endYear = date('Y', strtotime($month));

           $endDate = date('Y-m-d', strtotime($endYear."-".$endMonth."-15"));

           $tempDate = date('Y-m-d', strtotime($endDate. ' -1 months'));
           $startMonth = date('m', strtotime($tempDate));
           $startYear = date('Y', strtotime($tempDate));

           $startDate = date('Y-m-d', strtotime($startYear."-".$startMonth."-16"));

           $mrId = MonthlyReport::where('from', $startDate)
                   ->where('to', $endDate)
                   ->value('id');

            $from = date('F d, Y', strtotime($startDate));
            $to = date('F d, Y', strtotime($endDate));

           // HH Coverage
            $data = CsvData::select('barangay')
                ->selectRaw('COUNT(CASE WHEN client_status IN (1, 19, 24) THEN 1 WHEN client_status IS NULL THEN 0 ELSE NULL END) as hh_count')
                ->selectRaw('COUNT(CASE WHEN member_status IN (1, 19, 24) THEN 1 WHEN member_status IS NULL THEN 0 ELSE NULL END) as entry_count')
                ->where('muncity', 'LIKE', $selectedMuncity . '%')
                ->groupBy('barangay')
                ->get();

           $totalHHCount = $data->sum('hh_count');
           $totalEntryCount = $data->sum('entry_count');

            // HH Profile
            $profiledata = DB::table('h_h_profile')
                ->selectRaw('SUM(CASE WHEN profile = "Farmer, FW, FF" AND sex = "MALE" THEN 1 ELSE 0 END) AS farmer_male')
                ->selectRaw('SUM(CASE WHEN profile = "Farmer, FW, FF" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS farmer_female')
                ->selectRaw('SUM(CASE WHEN profile = "Farmer, FW, FF" THEN 1 ELSE 0 END) AS farmer_total')
                ->selectRaw('SUM(CASE WHEN profile = "Solo Parent" AND sex = "MALE" THEN 1 ELSE 0 END) AS solo_parent_male')
                ->selectRaw('SUM(CASE WHEN profile = "Solo Parent" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS solo_parent_female')
                ->selectRaw('SUM(CASE WHEN profile = "Solo Parent" THEN 1 ELSE 0 END) AS solo_parent_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Psychosocial" AND sex = "MALE" THEN 1 ELSE 0 END) AS psychosocial_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Psychosocial" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS psychosocial_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Psychosocial" THEN 1 ELSE 0 END) AS psychosocial_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Chronic Illness" AND sex = "MALE" THEN 1 ELSE 0 END) AS chronic_illness_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Chronic Illness" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS chronic_illness_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Chronic Illness" THEN 1 ELSE 0 END) AS chronic_illness_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Learning" AND sex = "MALE" THEN 1 ELSE 0 END) AS learning_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Learning" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS learning_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Learning" THEN 1 ELSE 0 END) AS learning_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Mental" AND sex = "MALE" THEN 1 ELSE 0 END) AS mental_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Mental" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS mental_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Mental" THEN 1 ELSE 0 END) AS mental_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Visual" AND sex = "MALE" THEN 1 ELSE 0 END) AS visual_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Visual" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS visual_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Visual" THEN 1 ELSE 0 END) AS visual_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Communication" AND sex = "MALE" THEN 1 ELSE 0 END) AS communication_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Communication" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS communication_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Communication" THEN 1 ELSE 0 END) AS communication_total')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Orthopedic" AND sex = "MALE" THEN 1 ELSE 0 END) AS orthopedic_male')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Orthopedic" AND sex = "FEMALE" THEN 1 ELSE 0 END) AS orthopedic_female')
                ->selectRaw('SUM(CASE WHEN profile = "PWD" AND pwd_type = "Orthopedic" THEN 1 ELSE 0 END) AS orthopedic_total')
                ->where('isActive', 1)
                ->first(); // Use first() to retrieve a single row of results

            $profileValues = [
                'farmer_male' => $profiledata->farmer_male,
                'farmer_female' => $profiledata->farmer_female,
                'farmer_total' => $profiledata->farmer_total,
                'solo_parent_male' => $profiledata->solo_parent_male,
                'solo_parent_female' => $profiledata->solo_parent_female,
                'solo_parent_total' => $profiledata->solo_parent_total,
                'psychosocial_male' => $profiledata->psychosocial_male,
                'psychosocial_female' => $profiledata->psychosocial_female,
                'psychosocial_total' => $profiledata->psychosocial_total,
                'chronic_illness_male' => $profiledata->chronic_illness_male,
                'chronic_illness_female' => $profiledata->chronic_illness_female,
                'chronic_illness_total' => $profiledata->chronic_illness_total,
                'learning_male' => $profiledata->learning_male,
                'learning_female' => $profiledata->learning_female,
                'learning_total' => $profiledata->learning_total,
                'mental_male' => $profiledata->mental_male,
                'mental_female' => $profiledata->mental_female,
                'mental_total' => $profiledata->mental_total,
                'visual_male' => $profiledata->visual_male,
                'visual_female' => $profiledata->visual_female,
                'visual_total' => $profiledata->visual_total,
                'communication_male' => $profiledata->communication_male,
                'communication_female' => $profiledata->communication_female,
                'communication_total' => $profiledata->communication_total,
                'orthopedic_male' => $profiledata->orthopedic_male,
                'orthopedic_female' => $profiledata->orthopedic_female,
                'orthopedic_total' => $profiledata->orthopedic_total,
            ];

            // Institutional Partnership
            $directoryData = MACDirectory::select('name', 'position', 'contact_number', 'email')
                ->where('user_id', $userId)
                ->where('isActive', true)
                ->get();

           $meetingData = MACMeeting::select('date_conducted', 'venue', 'agenda', 'resolution', 'date_sub_mom')
                ->where('mr_id', $mrId)
                ->where('isActive', true)
                ->get();

            $commitmentData = LGUCommitmentReport::select('barangay', 'ssa_gap', 'commitment', 'timeline', 'budget_source', 'status', 'remarks')
                    ->where('mr_id', $mrId)
                    ->where('isActive', true)
                    ->get();
           
           // Highlights of Activities
            $categories = [
                    'a. Attended activities related to the program (e.g. YDS, CSO orientation, MAC Meetings, etc)' => Activities::select('date_conducted', 'title', 'description', 'venue', 'objective', 'male', 'female', 'total', 'ip', 'fund_source', 'amount')->where('category', 'Program')->where('isActive', true)->get(),
                    'b. Augmentation to other program activities' => Activities::select('date_conducted', 'title', 'description', 'venue', 'objective', 'male', 'female', 'total', 'ip', 'fund_source', 'amount')->where('category', 'Other Program')->where('isActive', true)->get(),
                    'c. Attended LGU activities' => Activities::select('date_conducted', 'title', 'description', 'venue', 'objective', 'male', 'female', 'total', 'ip', 'fund_source', 'amount')->where('category', 'LGU')->where('isActive', true)->get(),
                ];



           /*$pdf = PDF::loadView('mr.pdf_mr_template', ['data' => $data, 'totalHHCount' => $totalHHCount, 'totalEntryCount' => $totalEntryCount, 'muncity' => $selectedMuncity, 'from' => $from, 'to' => $to, 'profileValues' => $profileValues, 'directoryData' => $directoryData, 'meetingData' => $meetingData, 'commitmentData' => $commitmentData, 'categories' => $categories])
                   ->setOptions([
                       'dpi' => 150,
                       'defaultFont' => 'Arial',
                       // Add more options here
                   ])
                   ->setPaper('A4', 'portrait') // Set page size and orientation
                   ->setOption('margin-top', 10) // Set top margin in millimeters
                   ->setOption('margin-right', 10) // Set right margin in millimeters
                   ->setOption('margin-bottom', 10) // Set bottom margin in millimeters
                   ->setOption('margin-left', 10); // Set left margin in millimeters
           
           return $pdf->download('mr.pdf');*/

           $imagePath1 = public_path('storage/images/dswd-car.png');
           $imagePath2 = public_path('storage/images/insigna.png');
           $imagePath3 = public_path('storage/images/4ps.png');
           $imagePath4 = public_path('storage/images/one-cordi.png');

           // Read the contents of the image file
           $dswdCarLogo = file_get_contents($imagePath1);
           $insignaLogo = file_get_contents($imagePath2);
           $pantawidLogo = file_get_contents($imagePath3);
           $oneCordiLogo = file_get_contents($imagePath4);

           // Create an instance of the DOMPDF class
           $dompdf = new Dompdf();
           $dompdf->set_option('isHtml5ParserEnabled', true);
           $dompdf->set_option('isRemoteEnabled', true);


           // Render the view with data and convert it to HTML
           $htmldata = view('mr.pdf_mr_template', [
               'data' => $data,
               'totalHHCount' => $totalHHCount,
               'totalEntryCount' => $totalEntryCount,
               'muncity' => $selectedMuncity,
               'from' => $from,
               'to' => $to,
               'profileValues' => $profileValues,
               'directoryData' => $directoryData,
               'meetingData' => $meetingData,
               'commitmentData' => $commitmentData,
               'categories' => $categories,
               'dswdCarLogo' => $dswdCarLogo,
               'insignaLogo' => $insignaLogo,
               'pantawidLogo' => $pantawidLogo,
               'oneCordiLogo' => $oneCordiLogo
           ])->render();

           // Load the HTML data into DOMPDF
           $dompdf->load_html($htmldata);

           // Configure the PDF (optional)
           $dompdf->set_option('dpi', 150);
           $dompdf->set_paper('A4', 'portrait');
           $dompdf->set_option('margin-top', 10);
           $dompdf->set_option('margin-right', 10);
           $dompdf->set_option('margin-bottom', 10);
           $dompdf->set_option('margin-left', 10);

           // Render the PDF
           $dompdf->render();

           // Get the raw PDF data
           $pdfdata = $dompdf->output();

           // You can then download the PDF using a response, save it to a file, etc.
           return response($pdfdata, 200)
               ->header('Content-Type', 'application/pdf')
               ->header('Content-Disposition', 'attachment; filename="mr.pdf"');

       }
}
