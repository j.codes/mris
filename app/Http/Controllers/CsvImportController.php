<?php

// app/Http/Controllers/CsvImportController.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Validator;
use League\Csv\Reader;
use Spatie\Async\Pool;
use App\Models\CsvData;

class CsvImportController extends Controller
{
    public function showForm()
    {
        if(Auth::user()){
            return view('mr/import');
        }
        else{
            return redirect('/login');
        } 
    }

    public function hh_roster(Request $request)
    {
        if(Auth::user()){

            $validator = Validator::make($request->all(), [
                'csv' => 'required|mimes:csv,txt',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $file = $request->file('csv');
            $csv = Reader::createFromPath($file->getRealPath(), 'r');

            $csvFields = ['region', 'province', 'muncity', 'barangay', 'hh_id', 'entry_id', 'first_name', 'mid_name', 'last_name', 'ext_name', 'bday', 'age', 'preg_status', 'hc_name', 'attend_school', 'dominant', 'grade_level', 'occupation', 'ip_affiliation', 'sex', 'rel_hh', 'member_status', 'disabled', 'child_bene', 'hh_grantee', 'hh_set', 'hh_set_grp', 'ed_attainment', 'school_name', 'registered', 'client_status', 'date_gen']; 

            $chunkSize = 500;
            $records = $csv->getRecords();
            $chunks = array_chunk(iterator_to_array($records), $chunkSize);
            
            $pool = Pool::create();

            foreach ($chunks as $chunk) {
                $pool->add(function () use ($chunk, $csvFields) {
                    foreach ($chunk as $row) {
                        $row = array_combine($csvFields, $row);
                        CsvData::create($row);
                    }
                })->catch(function (\Throwable $exception) {
                    // handle the exception
                    echo $exception->getMessage();
                });
            }

            $pool->wait();
                        
            return response()->json(['success' => 'CSV data imported successfully.'], 200);
        }
        else{
            return redirect('/login');
        } 
    }
}
