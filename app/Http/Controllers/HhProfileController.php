<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\HHProfile;

class HhProfileController extends Controller
{
    public function fetchHHMembers($hhId)
    { 
        $hhMembers = DB::table('h_h_roster')
            ->where('hh_id', $hhId)
            ->pluck(DB::raw("CONCAT(first_name, ' ', mid_name, ' ', last_name, ' ', ext_name) as full_name"), 'id');

        return response()->json($hhMembers);
    }

    public function fetchHHMemberSex(Request $request){
        $selectedFullname = $request->input('fullname');
        $sex = '';

        // Query the database to get the sex based on the selected fullname
        $result = DB::table('h_h_roster')
            ->select('sex')
            ->where(DB::raw("CONCAT(first_name, ' ', mid_name, ' ', last_name, ' ', ext_name)"), '=', $selectedFullname)
            ->first();

        if ($result) {
            $sex = $result->sex;
        }

        return $sex;
    }

    public function store(Request $request){
        if(Auth::user()){

            $prof = new HHProfile;
            
            /*$prof->mr_id = "";*/
            $prof->mr_id = $request->input('id');
            $prof->month = $request->input('month');
            $prof->year = $request->input('year');
            $prof->province_id = $request->input('province_id');
            $prof->mun_city_id = $request->input('mun_city_id');
            $prof->hh_id = $request->input('hh_id');
            $prof->sex = $request->input('sex');
            $prof->hh_member = $request->input('hh_member');
            $prof->profile = $request->input('profile');
            $prof->pwd_type = $request->input('pwd_type');
            $prof->user_id = (Auth::user()->id);
            $prof->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $prof = HHProfile::find($id);

        if(Auth::user()->id == $prof->user_id){
            $prof->hh_id = $request->input('hh_id');
            $prof->sex = $request->input('sex');
            $prof->hh_member = $request->input('hh_member');
            $prof->profile = $request->input('profile');
            $prof->pwd_type = $request->input('pwd_type');
            $prof->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function archive($id){
        $prof = HHProfile::find($id);

        if(Auth::user()->id == $prof->user_id){
            $prof->isActive = 0;
            $prof->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }
}
