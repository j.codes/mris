<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\MACMeeting;

class MACMeetingController extends Controller
{
    public function store(Request $request){
        if(Auth::user()){

            $meeting = new MACMeeting;
            
            $meeting->mr_id = $request->input('id');
            $meeting->month = $request->input('month');
            $meeting->year = $request->input('year');
            $meeting->province_id = $request->input('province_id');
            $meeting->mun_city_id = $request->input('mun_city_id');
            $meeting->date_conducted = $request->input('date_conducted');
            $meeting->venue = $request->input('venue');
            $meeting->agenda = $request->input('agenda');
            $meeting->resolution = $request->input('resolution');
            $meeting->date_sub_mom = $request->input('date_sub_mom');
            $meeting->user_id = (Auth::user()->id);
            $meeting->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $meeting = MACMeeting::find($id);

        if(Auth::user()->id == $meeting->user_id){
            $meeting->date_conducted = $request->input('date_conducted');
            $meeting->venue = $request->input('venue');
            $meeting->agenda = $request->input('agenda');
            $meeting->resolution = $request->input('resolution');
            $meeting->date_sub_mom = $request->input('date_sub_mom');
            $meeting->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function archive($id){
        $meeting = MACMeeting::find($id);

        if(Auth::user()->id == $meeting->user_id){
            $meeting->isActive = 0;
            $meeting->save();

            // return redirect('/mr');
        }
        else{
            return redirect('/login');
        }
    }
}
