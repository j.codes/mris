<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\MACDirectory;

class MACDirectoryController extends Controller
{
    public function store(Request $request){
        if(Auth::user()){

            $mac = new MACDirectory;
            
            $mac->mr_id = $request->input('id');
            $mac->month = $request->input('month');
            $mac->year = $request->input('year');
            $mac->province_id = $request->input('province_id');
            $mac->mun_city_id = $request->input('mun_city_id');
            $mac->name = $request->input('name');
            $mac->position = $request->input('position');
            $mac->contact_number = $request->input('contact');
            $mac->email = $request->input('email');
            $mac->user_id = (Auth::user()->id);
            $mac->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $mac = MACDirectory::find($id);

        if(Auth::user()->id == $mac->user_id){
            $mac->name = $request->input('name');
            $mac->position = $request->input('position');
            $mac->contact_number = $request->input('contact');
            $mac->email = $request->input('email');
            $mac->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function archive($id){
        $mac = MACDirectory::find($id);

        if(Auth::user()->id == $mac->user_id){
            $mac->isActive = 0;
            $mac->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }
}
