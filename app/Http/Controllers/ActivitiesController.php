<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Activities;

class ActivitiesController extends Controller
{
    public function store(Request $request){
        if(Auth::user()){

            $male = $request->input('male');
            $female = $request->input('female');
            $total = $male + $female;

            $activity = new Activities;
            
            $activity->mr_id = $request->input('id');
            $activity->month = $request->input('month');
            $activity->year = $request->input('year');
            $activity->province_id = $request->input('province_id');
            $activity->mun_city_id = $request->input('mun_city_id');
            $activity->category = $request->input('category');
            $activity->date_conducted = $request->input('date_conducted');
            $activity->title = $request->input('title');
            $activity->description = $request->input('description');
            $activity->venue = $request->input('venue');
            $activity->objective = $request->input('objective');
            $activity->male = $male;
            $activity->female = $female;
            $activity->total = $total;
            $activity->ip = $request->input('ip');
            $activity->fund_source = $request->input('fund_source');
            $activity->amount = $request->input('amount');
            $activity->user_id = (Auth::user()->id);
            $activity->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){

        $male = $request->input('male');
        $female = $request->input('female');
        $total = $male + $female;

        $activity = Activities::find($id);

        if(Auth::user()->id == $activity->user_id){
            $activity->category = $request->input('category');
            $activity->date_conducted = $request->input('date_conducted');
            $activity->title = $request->input('title');
            $activity->description = $request->input('description');
            $activity->venue = $request->input('venue');
            $activity->objective = $request->input('objective');
            $activity->male = $male;
            $activity->female = $female;
            $activity->total = $total;
            $activity->ip = $request->input('ip');
            $activity->fund_source = $request->input('fund_source');
            $activity->amount = $request->input('amount');
            $activity->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }

    public function archive($id){
        $activity = Activities::find($id);

        if(Auth::user()->id == $activity->user_id){
            $activity->isActive = 0;
            $activity->save();

            /*return redirect('/mr');*/
        }
        else{
            return redirect('/login');
        }
    }
}
