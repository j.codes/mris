<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HHProfile extends Model
{
    use HasFactory;

    protected $table = 'h_h_profile';

    protected $fillable = ['hh_id', 'hh_member', 'sex', 'profile', 'pwd_type', 'user_id', 'isActive'];
}
