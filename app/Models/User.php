<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function mrs(){
        return $this->hasMany('App\Models\MonthlyReport');
    }

    public function macdirectories(){
        return $this->hasMany('App\Models\MACDirectory');
    }

    public function macmeetings(){
        return $this->hasMany('App\Models\MACMeeting');
    }

    public function lgucommitmentreport(){
        return $this->hasMany('App\Models\LGUCommitmentReport');
    }

    public function activities(){
        return $this->hasMany('App\Models\Activities');
    }

    public function csvdata(){
        return $this->hasMany('App\Models\CsvData');
    }

    public function municipalcity(){
        return $this->hasMany('App\Models\MunicipalCity');
    }

    public function hhcoverage(){
        return $this->hasMany('App\Models\HHCoverage');
    }

}
