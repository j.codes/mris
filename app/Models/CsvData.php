<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CsvData extends Model
{
    use HasFactory;

    /*public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function mr(){
        return $this->belongsTo('App\Models\MonthlyReport');
    }*/

    protected $table = 'h_h_roster';

    protected $fillable = ['region', 'province', 'muncity', 'barangay', 'hh_id', 'entry_id', 'first_name', 'mid_name', 'last_name', 'ext_name', 'bday', 'age', 'preg_status', 'hc_name', 'attend_school', 'dominant', 'grade_level', 'occupation', 'ip_affiliation', 'sex', 'rel_hh', 'member_status', 'disabled', 'child_bene', 'hh_grantee', 'hh_set', 'hh_set_grp', 'ed_attainment', 'school_name', 'registered', 'client_status', 'date_gen'];
}
