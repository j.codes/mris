<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HHCoverage extends Model
{
    use HasFactory;

    protected $table = 'h_h_roster_stat';

    protected $fillable = ['mr_id', 'month', 'year', 'province_id', 'mun_city_id', 'c_1', 'c_19', 'c_24', 'total', 'isActive'];
}
