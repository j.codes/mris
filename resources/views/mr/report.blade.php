 @extends('layouts.app')

 @section('stylesheets')

     <style>
         .export-container {
             background-color: #fff !important;
             border: 1px solid #ccc;
             padding: 15px;
         }
         
         .mr-container1 .input-group-text {
             background-color: #555;
             color: #fff;
         }

         .mr-container1 select.form-control {
             border: 1px solid #555;
             color: #555;
         }

         .mr-container1 select.form-control:focus {
             border-color: #555;
             color: #555;
         }

         /* CSS for Upload Button */
         .export-mr #generate-pdf-btn.add-new {
             background-color: #555 !important;
             color: #fff !important; 
             border: none;
             border-radius: 4px;
             padding: 5px 5px;
             font-weight: bold;
             transition: background-color 0.3s, color 0.3s;
         }

         .export-mr #generate-pdf-btn.add-new:not([disabled]):hover {
             background-color: #fff !important; 
             color: #555 !important;
             border-style: solid !important;
             border-color: #555 !important;
         }
    
     </style>
 @endsection

 @section('content')
     <div class="export-container">
         <div class="mr-container1">
             <!-- Reporting Period Form -->
             <form method="POST" action="" class="mt-3">
                 <div class="my-3 row">
                     <!-- Province Dropdown -->
                     <div class="col-md-3">
                         <div class="input-group">
                             <span class="input-group-text">Province:</span>
                             <select aria-label="province" name="mr-province" id="mr-province" class="form-control" onchange="updateMuncityDropdown()">
                                 <option value="" selected>Select Province</option>
                                 @foreach($provincesAndMuncities as $province => $muncities)
                                 <option value="{{ $province }}">{{ $province }}</option>
                                 @endforeach
                             </select>
                         </div>
                     </div>
                     <!-- Municipality Dropdown -->
                     <div class="col-md-3">
                         <div class="input-group">
                             <span class="input-group-text">Municipality:</span>
                             <select aria-label="municipality" name="mr-muncity" id="mr-muncity" class="form-control">
                                 <option value="" selected>Select Municipality</option>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-3">
                         <div class="input-group">
                             <span class="input-group-text">Month:</span>
                             <input type="month" aria-label="date-month" name="date-month" id="date-month" class="form-control" value="">
                         </div>
                     </div>
                 </div>
             </form>
         </div>

         <div class="export-mr" style="border: 1px solid #ccc; padding: 15px; border-radius: 5px;">
             <h2 style="margin-bottom: 15px;">MR Export</h2>
             <form id="pdf-generation-form" method="get" enctype="multipart/form-data">
                 @csrf
                 <input type="hidden" name="muncity" id="hidden-muncity" value="">
                 <input type="hidden" name="month" id="hidden-month" value="">
                 <fieldset>
                     <label style="font-weight: bold;">Generate PDF:</label>
                     <button type="submit" id="generate-pdf-btn" class="add-new" disabled>Generate PDF</button>
                     <p id="warning-message" style="color: red; display: none;">Please select a muncity and a month.</p>
                 </fieldset>
             </form>
         </div>
     </div>
 @endsection

 @push('scripts')
     <script> 

         /*For the muncity dropdown*/
         function updateMuncityDropdown() {
              var provinceDropdown = document.getElementById('mr-province');
              var muncityDropdown = document.getElementById('mr-muncity');
              var selectedProvince = provinceDropdown.value;

              // Clear existing options
              muncityDropdown.innerHTML = '<option value="" selected>Select Municipality</option>';

              // Get the municipalities for the selected province
              var muncities = @json($provincesAndMuncities);

              if (selectedProvince && muncities[selectedProvince]) {
                  muncities[selectedProvince].forEach(function(muncity) {
                      var option = document.createElement('option');
                      option.value = muncity.muncity;
                      option.textContent = muncity.muncity;
                      muncityDropdown.appendChild(option);
                  });
              }
          }

          function updatePdfFormAction() {
              const selectedMProvince = document.getElementById('mr-province').value;
              const selectedMuncity = document.getElementById('mr-muncity').value;
              const selectedMonth = document.getElementById('date-month').value;
              const pdfForm = document.getElementById('pdf-generation-form');
              const hiddenMuncityInput = document.getElementById('hidden-muncity');
              const hiddenMonthInput = document.getElementById('hidden-month');
              const generatePdfButton = document.getElementById('generate-pdf-btn');
              
              if (selectedMProvince && selectedMuncity && selectedMonth) {
                  pdfForm.action = '/generateMR/' + selectedMuncity;
                  hiddenMuncityInput.value = selectedMuncity;
                  hiddenMonthInput.value = selectedMonth;
                  generatePdfButton.disabled = false;
              } else {
                  pdfForm.action = '';
                  hiddenMuncityInput.value = '';
                  hiddenMonthInput.value = '';
                  generatePdfButton.disabled = true;
              }
          }

          // Call the function when the muncity dropdown or month input changes
          document.getElementById('mr-muncity').addEventListener('change', updatePdfFormAction);
          document.getElementById('date-month').addEventListener('change', updatePdfFormAction);

          // Call the function initially to set the correct action based on the initial values
          updatePdfFormAction();

     </script>
  @endpush

