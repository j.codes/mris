@extends('layouts.app')

@section('stylesheets')

    <style>
        /* CSS for CSV Import Container */
        .csv-import-container {
            background-color: #fff !important;
            border: 1px solid #ccc;
            padding: 15px;
        }

        /* CSS for Import Title */
        .import-title {
            color: #555;
        }

        /* CSS for File Label */
        .file-label {
            color: #555;
        }

        /* CSS for Upload Button */
        fieldset button.upload-button {
            background-color: #555 !important;
            color: #fff !important; 
            border: none;
            border-radius: 4px;
            padding: 5px 5px;
            font-weight: bold;
            transition: background-color 0.3s, color 0.3s;
        }

        fieldset button.upload-button:not([disabled]):hover {
            background-color: #fff !important; 
            color: #555 !important;
            border-style: solid !important;
            border-color: #555 !important;
        }
   
    </style>
@endsection

@section('content')
    <!-- resources/views/csv_import_form.blade.php -->
    <div class="csv-import-container">
        <h2 class="import-title">CSV Import</h2>
        <form action="/import" method="post" enctype="multipart/form-data">
            @csrf
            <fieldset>
                <label for="csvFile" class="file-label">Choose a CSV File:</label>
                <input type="file" id="csvFile" name="csv" accept=".csv">
                <button type="submit" class="upload-button" disabled>Upload</button>
            </fieldset>
        </form>
    </div>

    @if (session('success'))
        <p>{{ session('success') }}</p>
    @endif  

@endsection

@push('scripts')
    <script>
        // Function to enable or disable the upload button
        function toggleUploadButton() {
            const fileInput = document.getElementById('csvFile');
            const uploadButton = document.querySelector('.upload-button');

            if (fileInput.files.length > 0) {
                uploadButton.disabled = false;
            } else {
                uploadButton.disabled = true;
            }
        }

        // Attach an event listener to the file input for change events
        document.getElementById('csvFile').addEventListener('change', toggleUploadButton);

        // Call the function initially to set the button's initial state
        toggleUploadButton();
    </script>

 @endpush
