<!DOCTYPE html>
<html>
<head>
    <title>PDF Template</title>
    <style>
        /* Add your CSS styles here */
        @page {
            header: page-header;
        }
        body {
            font-family: Arial, sans-serif;
        }
        .header-container {
            overflow: auto;
        }
        .logo-container {
            float: left;
            height: 75px;
            margin: 0px 0 0 10px;
        }
        .logo-container2 {
            float: right;
            height: 75px;
            margin: 0px 10px 0 0;
        }
        .logo-wrapper {
            margin-top: 15px;
        }
        .logo {
            height: 75px;
            padding: 0 5px 0 0;
            border-right: 3px solid black;
            margin: 0 5px 0 0;
        }
        .logo-wrapper img:last-of-type,
        .logo-container2 img:last-of-type {
            border-right: none;
            margin-right: 0;
        }
        .logo-text {
            margin: 80px 0 0 10px;
            padding: 0 0 0 0;
            font-size: 12px;
            color: blue;
        }
        .header {
            text-align: center;
            margin: 20px 0 20px 0;
        }
        .heading {
            margin: 0 0 0 0;
        }
        p {
            font-size: 16px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th {
            background-color: #FFA233;
            text-align: center; 
            font-size: 20px;
            font-weight: bold;
        }
        td {
            text-align: left;
            font-size: 16px;
        }
        th, td {
            border: 1px solid #000;
            padding: 5px;
        }
        .total {
            background-color: #FFFF00;
            font-size: 18px;
            font-weight: bold;
        }
        .coverage-column {
            width: 25%;
        }
        .profile-column {
            width: 16.66667%;
        }
        #signatories {
            overflow: auto;
        }
        .signatory {
            width: 30%;
            float: left; 
            margin-right: 20%; 
            padding: 10px; 
        }
        .signatory hr {
            border-color: #000;
            margin: 0 auto;
        }
        .clear {
            clear: both; 
        }

    </style>
</head>

<htmlpageheader name="page-header">
    <div class="header-container">
        <div class="logo-container">
            <div class="logo-wrapper">
                <img src="data:image/jpeg;base64,{{ base64_encode($dswdCarLogo) }}" alt="DSWD-CAR logo" class="logo">
                <img src="data:image/jpeg;base64,{{ base64_encode($insignaLogo) }}" alt="Insigna logo" class="logo">
                <img src="data:image/jpeg;base64,{{ base64_encode($pantawidLogo) }}" alt="4Ps logo" class="logo">   
            </div>         
        </div>
        <div class="logo-container2">
            <img src="data:image/jpeg;base64,{{ base64_encode($oneCordiLogo) }}" alt="One Cordillera logo" class="logo">
        </div>

        <p class="logo-text">"Serbisyong Maagap, Mapagkalinga at Tapat tungo sa Matatag na Kordilyera"</p>
        <div class="clear"></div> <!-- Clear the floats to prevent layout issues -->
    </div>
</htmlpageheader>


<body>
    <div class="header">
        <h4 class="heading">MUNICIPALITY OF: {{$muncity}}</h4>
        <h4 class="heading">COVERAGE PERIOD: {{$from}} to {{$to}}</h4>
    </div>

    <h4>I. Household Coverage</h4>

    <table>
        <thead>
            <tr>
                <th>Barangay</th>
                <th class="coverage-column">Number of Active HHs (Codes 1, 19, 24)</th>
                <th class="coverage-column">Number of Active HH Members (Codes 1, 19, 24)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
                <tr>
                    <td>{{$row->barangay}}</td>
                    <td>{{$row->hh_count}}</td>
                    <td>{{$row->entry_count}}</td>
                </tr>
            @endforeach
            	<tr class="total">
	                <td>TOTAL</td>
	                <td>{{$totalHHCount}}</td>
	                <td>{{$totalEntryCount}}</td>
	            </tr>
        </tbody>
    </table>

    <h4>II. Household Profile</h4>

    <table>
        <thead>
            <tr>
                <th>Profile</th>
                <th class="profile-column">No. of Male</th>
                <th class="profile-column">No. of Female</th>
                <th class="profile-column">Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4">HH members who are Person/s With Disability (PWD/s)</td>
            </tr>
            <tr>
                <td>Psychosocial</td>
                <td>{{ $profileValues['psychosocial_male'] }}</td>
                <td>{{ $profileValues['psychosocial_female'] }}</td>
                <td>{{ $profileValues['psychosocial_total'] }}</td>
            </tr>
            <tr>
                <td>Chronic Illness</td>
                <td>{{ $profileValues['chronic_illness_male'] }}</td>
                <td>{{ $profileValues['chronic_illness_female'] }}</td>
                <td>{{ $profileValues['chronic_illness_total'] }}</td>
            </tr>
            <tr>
                <td>Learning</td>
                <td>{{ $profileValues['learning_male'] }}</td>
                <td>{{ $profileValues['learning_female'] }}</td>
                <td>{{ $profileValues['learning_total'] }}</td>
            </tr>
            <tr>
                <td>Mental</td>
                <td>{{ $profileValues['mental_male'] }}</td>
                <td>{{ $profileValues['mental_female'] }}</td>
                <td>{{ $profileValues['mental_total'] }}</td>
            </tr>
            <tr>
                <td>Visual</td>
                <td>{{ $profileValues['visual_male'] }}</td>
                <td>{{ $profileValues['visual_female'] }}</td>
                <td>{{ $profileValues['visual_total'] }}</td>
            </tr>
            <tr>
                <td>Communication</td>
                <td>{{ $profileValues['communication_male'] }}</td>
                <td>{{ $profileValues['communication_female'] }}</td>
                <td>{{ $profileValues['communication_total'] }}</td>
            </tr>
            <tr>
                <td>Orthopedic</td>
                <td>{{ $profileValues['orthopedic_male'] }}</td>
                <td>{{ $profileValues['orthopedic_female'] }}</td>
                <td>{{ $profileValues['orthopedic_total'] }}</td>
            </tr>
            <tr>
                <td>HH members engaged as Farmer/Forest Worker/Fisher Folk</td>
                <td>{{ $profileValues['farmer_male'] }}</td>
                <td>{{ $profileValues['farmer_female'] }}</td>
                <td>{{ $profileValues['farmer_total'] }}</td>
            </tr>
            <tr>
                <td>HH members who are Solo Parents</td>
                <td>{{ $profileValues['solo_parent_male'] }}</td>
                <td>{{ $profileValues['solo_parent_female'] }}</td>
                <td>{{ $profileValues['solo_parent_total'] }}</td>
            </tr>
        </tbody>
    </table>

    <h4>III. Institutional Partneship and Development</h4>

    <h5>a. Directory of MAC Members and LGU Link</h5>

    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Contact Number</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($directoryData as $entry)
                <tr>
                    <td>{{$entry->name}}</td>
                    <td>{{$entry->position}}</td>
                    <td>{{$entry->contact_number}}</td>
                    <td>{{$entry->email}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h5>b. Municipal Advisory Committee Meetings / Municipal Interagency Meetings conducted</h5>

    <table>
        <thead>
            <tr>
                <th>Date Conducted</th>
                <th>Venue</th>
                <th>Agenda</th>
                <th>Resolution</th>
                <th>Date of Submission of Minutes</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($meetingData as $meeting)
                <tr>
                    <td>{{$meeting->date_conducted}}</td>
                    <td>{{$meeting->venue}}</td>
                    <td>{{$meeting->agenda}}</td>
                    <td>{{$meeting->resolution}}</td>
                    <td>{{$meeting->date_sub_mom}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h5>c. LGU Commitment Report</h5>

    <table>
        <thead>
            <tr>
                <th>Barangay</th>
                <th>SSA Gap</th>
                <th>Commitment</th>
                <th>Timeline</th>
                <th>Budget Source</th>
                <th>Status</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($commitmentData as $entry)
                <tr>
                    <td>{{$entry->barangay}}</td>
                    <td>{{$entry->ssa_gap}}</td>
                    <td>{{$entry->commitment}}</td>
                    <td>{{$entry->timeline}}</td>
                    <td>{{$entry->budget_source}}</td>
                    <td>{{$entry->status}}</td>
                    <td>{{$entry->remarks}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h4>IV. Highlights of Activities</h4>

    @foreach ($categories as $category => $activityData)
        <h5>{{$category}}</h5>
        @include('mr.activity_table', ['activityData' => $activityData])
    @endforeach

    <br> <br>

    <div id="signatories">
        <div class="signatory" id="prepared-by">
            <p>Prepared by:</p>
            <br>
            <br>
            <hr>
            <p>PDO II - City/Municipal Link</p>
        </div>
        <div class="signatory" id="reviewed-by">
            <p>Reviewed by:</p>
            <br>
            <br>
            <hr>
            <p>Social Welfare Officer III</p>
        </div>
        <div class="clear"></div> <!-- Clear the floats to prevent layout issues -->
    </div>

</body>
</html>