@extends('layouts.app')

@section('stylesheets')

    <style>
		/* Default styles for larger screens */
		.mr-container {
		    width: 100%;
		    max-height: 80%; 
		    overflow-y: auto;
		    margin: 0 auto;
		    padding: 20px;
		    background-color: #fff;
		    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
		}

		/* Media query for medium-sized screens */
		@media (max-width: 768px) {
		    .mr-container {
		        width: 100%;
		    }
		}

		/* Media query for small screens */
		@media (max-width: 576px) {
		    .mr-container {
		        width: 90vw;
		        padding: 10px;
		    }
		}

		/* Table Title Styles */
		.table-title {
		    padding: 20px 0 10px;
		    margin: 0;
		    font-size: 22px;
		}

		.input-group .input-group-text {
		    background-color: #555;
		    color: #fff;
		}

		/* Table Styles */
		table.table {
		    width: 100%;
		    border-collapse: collapse;
		    table-layout: fixed;
		    margin-bottom: 20px;
		}

		table.table th,
		table.table td {
		    border: 1px solid #ddd;
		    padding: 10px;
		    text-align: left;
		}

		table.table th {
		    background-color: #f5f5f5;
		    font-weight: bold;
		}

		table.table th i {
		    font-size: 16px;
		    margin: 0 5px;
		    cursor: pointer;
		}

		table.table th:last-child {
		    width: 100px;
		}

		table.table td a {
		    color: #3498db;
		    text-decoration: none;
		    margin: 0 5px;
		}

		table.table td a.add {
		    color: #27C46B;
		}

		table.table td a.edit {
		    color: #FFC107;
		}

		table.table td a.delete {
		    color: #E34724;
		}

		table.table td i {
		    font-size: 18px;
		}

		table.table td a.add i {
		    font-size: 20px;
		    margin-right: 5px;
		    position: relative;
		    top: 2px;
		}

		/* Form Control Styles */
		table.table .form-control {
		    width: 100%;
		    line-height: 32px;
		    box-shadow: none;
		    border: 1px solid #ddd;
		    border-radius: 4px;
		}

		table.table .form-control.error {
		    border-color: #f50000;
		}

		 table.table td .add {
	        display: none;
	    }

	    table.table select {
	    		width: 100%;
	    	  	max-width: 100%;
	    }

		/*Add New Button Styles*/
		.add-new-container {
			 text-align: right;
		}

		.add-new {
		    background-color: #555 !important;
		    color: #fff !important; 
		    border: none;
		    border-radius: 4px;
		    padding: 10px 20px;
		    font-weight: bold;
		    transition: background-color 0.3s, color 0.3s;
		}

		.add-new:hover {
		    background-color: #fff !important; 
		    color: #555 !important;
		    border-style: solid !important;
		    border-color: #555 !important;
		}

		/*Nav Styles*/
		.nav-link.custom-link {
		    background-color: #fff;
		    color: #555;
		    border: none;
		    border-radius: 0; 
		    padding: 10px 20px;
		    font-weight: bold;
		    transition: background-color 0.3s, color 0.3s;
		}

		.nav-link.custom-link:hover {
		    background-color: #555; 
		    color: #fff; 
		}

		/* Disabled tab styles */
		.nav-link.custom-link.disabled {
		    cursor: not-allowed; 
		    background-color: #555;
		    color: #888 !important; 
		}

		/* Active tab indicator */
		.nav-link.active.custom-link::before {
		    content: "";
		    position: absolute;
		    bottom:
		}

		/* Additional Styles */
		.small-legend {
		    font-size: 1rem;
		}

		.fieldset-custom {
		    width: 70%;
		    margin: 0 auto;
		}
    </style>
@endsection

@section('content')
	<div class="main-container">
		<div class="container-fluid">
		    <nav>
		        <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
		            <button class="nav-link custom-link active" id="nav-period-tab" data-bs-toggle="tab" data-bs-target="#nav-period" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Period</button>
		            <button class="nav-link custom-link disabled" id="nav-hhcoverage-tab" data-bs-toggle="tab" data-bs-target="#nav-hhcoverage" type="button" role="tab" aria-controls="nav-hhcoverage" aria-selected="false">HH Coverage</button>
		            <button class="nav-link custom-link disabled" id="nav-hhprofile-tab" data-bs-toggle="tab" data-bs-target="#nav-hhprofile" type="button" role="tab" aria-controls="nav-hhprofile" aria-selected="false">HH Profile</button>
		            <button class="nav-link custom-link disabled" id="nav-macdirectory-tab" data-bs-toggle="tab" data-bs-target="#nav-macdirectory" type="button" role="tab" aria-controls="nav-macdirectory" aria-selected="false">MAC Directory</button>
		            <button class="nav-link custom-link disabled" id="nav-macmiacmeeting-tab" data-bs-toggle="tab" data-bs-target="#nav-macmiacmeeting" type="button" role="tab" aria-controls="nav-macmiacmeeting" aria-selected="false">MAC/MIAC Meetings Conducted</button>
		            <button class="nav-link custom-link disabled" id="nav-lgucommitment-tab" data-bs-toggle="tab" data-bs-target="#nav-lgucommitment" type="button" role="tab" aria-controls="nav-lgucommitment" aria-selected="false">LGU Commitment Report</button>
		            <button class="nav-link custom-link disabled" id="nav-activities-tab" data-bs-toggle="tab" data-bs-target="#nav-activities" type="button" role="tab" aria-controls="nav-activities" aria-selected="false">Highlights of Activities</button>
		        </div>
		    </nav>
		</div>

		<div class="tab-content px-1 mx-1" id="nav-tabContent">
		    <div class="tab-pane fade show active" id="nav-period" role="tabpanel" aria-labelledby="nav-period-tab" tabindex="0">
		    	<div class="nav-period">
		        <div class="mr-container">
		        		<div class="mr-container1">
			            <!-- Reporting Period Form -->
			            <form method="POST" action="/mr/period" class="mt-3">
			                @csrf
			                <div class="row">
			                    <div class="col-md-12">
			                        <h3>Reporting Period</h3>
			                    </div>
			                </div>
			                <div class="my-3 row">
			                    <!-- Province Dropdown -->
			                    <div class="col-md-4">
			                        <div class="input-group">
			                            <span class="input-group-text">Province:</span>
			                            <select aria-label="province" name="mr-province" id="mr-province" class="form-control" onchange="updateMuncityDropdown()">
			                                <option value="" selected>Select Province</option>
			                                @foreach($provincesAndMuncities as $province => $muncities)
			                                    <option value="{{ $province }}">{{ $province }}</option>
			                                @endforeach
			                            </select>
			                        </div>
			                    </div>
			                    <!-- Municipality Dropdown -->
			                    <div class="col-md-4">
			                        <div class="input-group">
			                            <span class="input-group-text">Municipality:</span>
			                            <select aria-label="municipality" name="mr-muncity" id="mr-muncity" class="form-control">
			                                <option value="" selected>Select Municipality</option>
			                            </select>
			                        </div>
			                    </div>
			                    <!-- Month Input -->
			                    <div class="col-md-3">
			                        <div class="input-group">
			                            <span class="input-group-text">Month:</span>
			                            <input type="month" aria-label="date-month" name="date-month" id="date-month" class="form-control" value="">
			                        </div>
			                    </div>
			                    <!-- Generate Button -->
			                    <div class="col-md-1">
			                        <button type="submit" class="btn add-new">Generate</button>
			                    </div>
			                </div>
			            </form>
		            </div>

		            <!-- List of Monthly Reports -->
		            <div class="mr-container2">
		                <div class="table-responsive">
		                    <table class="table table-bordered" id="table-mr">
		                        <thead>
		                            <tr>
		                                <th hidden id="latest_mr_id">{{$lastmrid}}</th>
		                                <th>Province</th>
		                                <th>Municipality</th>
		                                <th>From</th>
		                                <th>To</th>
		                                <th>Status</th>
		                                <th>Actions</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            @if(count($monthlyreports) > 0)
		                                @foreach($monthlyreports as $mr)
		                                    <tr>
		                                        <td hidden> {{$mr->id}} </td>
		                                        <td> {{$mr->province_id}} </td>
		                                        <td> {{$mr->mun_city_id}} </td>
		                                        <td> {{$mr->from}} </td>
		                                        <td> {{$mr->to}} </td>
		                                        <td>{{ $mr->isActive ? 'Active' : 'Inactive' }}</td>
		                                        <td>
		                                            <a class="edit" id="edit-mr" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
		                                            <a class="delete" id="delete-mr" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
		                                        </td>
		                                    </tr> 
		                                @endforeach
		                            @else
		                                <tr hidden>
		                                    <td> </td>
		                                    <td> </td>
		                                    <td> </td>
		                                    <td> </td>
		                                    <td> </td>
		                                    <td>
		                                        <a class="edit" id="edit-mr" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
		                                        <a class="delete" id="delete-mr" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
		                                    </td>
		                                </tr>
		                            @endif    
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		      </div>
		    </div>

		    <div class="tab-pane fade" id="nav-hhcoverage" role="tabpanel" aria-labelledby="nav-hhcoverage-tab" tabindex="0">
		        <form id="hhCoverageForm" method="POST" action="/mr/coverage" class="mt-3">
		            @csrf

		            <input type="hidden" id="mrid" name="mrid">
		            <input type="hidden" id="month" name="month">
		            <input type="hidden" id="year" name="year">
		            <input type="hidden" id="province_id" name="province_id">
		            <input type="hidden" id="mun_city_id" name="mun_city_id">
		            <input type="hidden" id="c1" name="c1">
		            <input type="hidden" id="c19" name="c19">
		            <input type="hidden" id="c24" name="c24">
		            <input type="hidden" id="tcount" name="tcount">

		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>Household Coverage</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="submit" class="btn custom-btn add-new" id="save-roster">
		                                    	 Save  
		                                    </button>
		                                </div>
		                            </div>
		                        </div>
		                        <table class="table table-bordered" id="table-hhcoverage">
		                            <thead>
		                                <tr>
		                                    <th style="text-align: center; vertical-align: middle; width: 50%;">Barangay</th>
		                                    <th style="text-align: center; vertical-align: middle; width: 25%;">Number of Active HHs</th>
		                                    <th style="text-align: center; vertical-align: middle; width: 25%;">Number of Active Members</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                               
		                            </tbody>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>

		    <div class="tab-pane fade" id="nav-hhprofile" role="tabpanel" aria-labelledby="nav-hhprofile-tab" tabindex="0">
		        {{-- <input type="hidden" id="id" name="id" value="{{ $selectedmr->id ?? '' }}">
		        <input type="hidden" id="month" name="month" value="{{ $selectedmr->month ?? '' }}">
		        <input type="hidden" id="year" name="year" value="{{ $selectedmr->year ?? '' }}">
		        <input type="hidden" id="province_id" name="province_id" value="{{ $selectedmr->province_id ?? '' }}">
		        <input type="hidden" id="mun_city_id" name="mun_city_id" value="{{ $selectedmr->id ?? '' }}"> --}}

		        <form method="POST" action="/mr/profile" class="mt-3">
		            @csrf

		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>Household Profile</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="button" class="btn custom-btn add-new" id="add-new-profile">
		                                        <i class="fa fa-plus"></i> Add New
		                                    </button>
		                                </div>
		                            </div>
		                        </div>
		                        <table class="table table-bordered" id="table-hhprofile">
		                            <thead>
		                                <tr>
		                                    <th hidden id="latest_profile_id">{{$lasthhprofileid}}</th>
		                                    <th>HH ID</th>
		                                    <th>HH Member</th>
		                                    <th>Profile</th>
		                                    <th>Type (for PWD)</th>
		                                    <th>Actions</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                @if(count($hhprofiles) > 0)
		                                    @foreach($hhprofiles as $hhprofile)
		                                        <tr>
		                                            <td hidden>{{$hhprofile->id}}</td>
		                                            <td hidden>{{$hhprofile->sex}}</td>
		                                            <td>{{$hhprofile->hh_id}}</td>
		                                            <td>{{$hhprofile->hh_member}}</td>
		                                            <td>{{$hhprofile->profile}}</td>
		                                            <td>{{$hhprofile->pwd_type}}</td>
		                                            <td>
		                                                <a class="add" id="add-profile" title="Add" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE03B;</i>
		                                                </a>
		                                                <a class="edit" id="edit-profile" title="Edit" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE254;</i>
		                                                </a>
		                                                <a class="delete" id="delete-profile" title="Delete" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE872;</i>
		                                                </a>
		                                            </td>
		                                        </tr>
		                                    @endforeach
		                                @else
		                                    <tr hidden>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td>
		                                            <a class="add" id="add-profile" title="Add" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE03B;</i>
		                                            </a>
		                                            <a class="edit" id="edit-profile" title="Edit" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE254;</i>
		                                            </a>
		                                            <a class="delete" id="delete-profile" title="Delete" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE872;</i>
		                                            </a>
		                                        </td>
		                                    </tr>
		                                @endif
		                            </tbody>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>

		    <div class="tab-pane fade" id="nav-macdirectory" role="tabpanel" aria-labelledby="nav-macdirectory-tab" tabindex="0">
		        <form method="POST" action="/mr/mac" class="mt-3">
		            @csrf

		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>Directory of MAC Members and LGU Link</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="button" class="btn custom-btn add-new" id="add-new-mac">
		                                        <i class="fa fa-plus"></i> Add New
		                                    </button>
		                                </div>
		                            </div>
		                        </div>
		                        <table class="table table-bordered" id="table-mac">
		                            <thead>
		                                <tr>
		                                    <th hidden id="latest_mac_id">{{$lastmacid}}</th>
		                                    <th>Name</th>
		                                    <th>Position</th>
		                                    <th>Contact No.</th>
		                                    <th>Email</th>
		                                    <th>Actions</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                @if(count($macdirectories) > 0)
		                                    @foreach($macdirectories as $macmember)
		                                        <tr>
		                                            <td hidden>{{$macmember->id}}</td>
		                                            <td>{{$macmember->name}}</td>
		                                            <td>{{$macmember->position}}</td>
		                                            <td>{{$macmember->contact_number}}</td>
		                                            <td>{{$macmember->email}}</td>
		                                            <td>
		                                                <a class="add" id="add-mac" title="Add" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE03B;</i>
		                                                </a>
		                                                <a class="edit" id="edit-mac" title="Edit" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE254;</i>
		                                                </a>
		                                                <a class="delete" id="delete-mac" title="Delete" data-toggle="tooltip">
		                                                    <i class="material-icons">&#xE872;</i>
		                                                </a>
		                                            </td>
		                                        </tr>
		                                    @endforeach
		                                @else
		                                    <tr hidden>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td></td>
		                                        <td>
		                                            <a class="add" id="add-mac" title="Add" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE03B;</i>
		                                            </a>
		                                            <a class="edit" id="edit-mac" title="Edit" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE254;</i>
		                                            </a>
		                                            <a class="delete" id="delete-mac" title="Delete" data-toggle="tooltip">
		                                                <i class="material-icons">&#xE872;</i>
		                                            </a>
		                                        </td>
		                                    </tr>
		                                @endif
		                            </tbody>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>

		    <div class="tab-pane fade" id="nav-macmiacmeeting" role="tabpanel" aria-labelledby="nav-macmiacmeeting-tab" tabindex="0">
		        <form method="POST" action="/mr_meeting" class="mt-3">
		            @csrf

		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>Municipal Advisory Committee Meetings / Municipal Interagency Meetings Conducted</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="button" class="btn custom-btn add-new" id="add-new-meeting">
		                                        <i class="fa fa-plus"></i> Add New
		                                    </button>
		                                </div>
		                            </div>
		                        </div>

		                        <table class="table table-bordered" id="table-meeting">
                                <thead>
                                    <tr>
                                        <th hidden id="latest_meeting_id"></th>
                                        <th>Date</th>
                                        <th>Venue</th>
                                        <th>Agenda / Agreements</th>
                                        <th>Resolutions Crafted and Approved</th>
                                        <th>Date of Submission of MOM</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Table rows will be populated by JavaScript -->
                                </tbody>
                            	</table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>

		    <div class="tab-pane fade" id="nav-lgucommitment" role="tabpanel" aria-labelledby="nav-lgucommitment-tab" tabindex="0">
		        <form method="POST" action="/mr_lgucommitment" class="mt-3">
		            @csrf
		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>LGU Commitment Report</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="button" class="btn custom-btn add-new" id="add-new-lgucommitment">
		                                        <i class="fa fa-plus"></i> Add New
		                                    </button>
		                                </div>
		                            </div>
		                        </div>
		                        <table class="table table-bordered" id="table-lgucommitment">
		                            <thead>
		                                <tr>
		                                    <th hidden id="latest_lgucommitment_id"></th> <!-- This will be populated by JavaScript -->
		                                    <th>Barangay</th>
		                                    <th>Identified SSA GAP</th>
		                                    <th>Commitment / Intervention of LGU</th>
		                                    <th>Timeline</th>
		                                    <th>Source of Budget</th>
		                                    <th>Status</th>
		                                    <th>Remarks</th>
		                                    <th>Action</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                <!-- Table will be populated by JavaScript -->
		                            </tbody>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>

		    <div class="tab-pane fade" id="nav-activities" role="tabpanel" aria-labelledby="nav-activities-tab" tabindex="0">
		        <form method="POST" action="/mr_activities" class="mt-3">
		            @csrf

		            <div class="mr-container d-flex justify-content-center">
		                <div class="table-responsive">
		                    <div class="table-wrapper">
		                        <div class="table-title">
		                            <div class="row">
		                                <div class="col-sm-10">
		                                    <h3>Highlights of Activities</h3>
		                                </div>
		                                <div class="col-sm-2 add-new-container">
		                                    <button type="button" class="btn custom-btn add-new" id="add-new-activity">
		                                        <i class="fa fa-plus"></i> Add New
		                                    </button>
		                                </div>
		                            </div>
		                        </div>
		                        <table class="table table-bordered" id="table-activities">
		                            <thead>
		                                <tr>
		                                    <th hidden id="latest_activity_id">{{$lastactivityid}}</th>
		                                    <th>Category</th>
		                                    <th>Inclusive Dates</th>
		                                    <th>Title of the Activity</th>
		                                    <th>Description</th>
		                                    <th>Venue</th>
		                                    <th>Objective / Output</th>
		                                    <th>Male</th>
		                                    <th>Female</th>
		                                    <th>IP</th>
		                                    <th>Fund Source</th>
		                                    <th>Amount</th>
		                                    <th>Action</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                <!-- Table body here, which can be populated with your existing PHP code or via JavaScript/jQuery -->
		                            </tbody>
		                        </table>
		                    </div>
		                </div>
		            </div>
		        </form>
		    </div>
		</div>
	</div>
@endsection

@push('scripts')
    <script> 

    	var current_mrid; 
    	var month; 
    	var year; 
    	var province_id;
    	var mun_city_id; 
    	
	    $(document).ready(function(){

	    	/*var current_mrid = document.getElementById("id").value;
	    	var month = document.getElementById("month").value;
	    	var year = document.getElementById("year").value;
	    	var province_id = document.getElementById("province_id").value;
	    	var mun_city_id = document.getElementById("mun_city_id").value;*/

	    // For the MR
	    	$('[data-toggle="tooltip"]').tooltip();

	    	
	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-mr", function(){

	    		var currentRow = $(this).closest("tr"); 
	    		var id = currentRow.find("td:eq(0)").text();
	    		var id2 = id.trim();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
				    url: "/mr/period/" + id2,
				    type: "GET",
				    success: function(response) {
				        if (response) {
				            // Update form input fields with the fetched data
				        		$('#id').val(response.id);

				            current_mrid = response.id;
				            month = response.month;
				            year = response.year;
				            province_id = response.province_id;
				            mun_city_id = response.mun_city_id;

				            $('#hhprofile-form').attr('action', '/mr/hh_profile/' + response.id)

				            var lastMeetingId = response.lastmacmeetingid;
				            				            
				            populateMeetingTable(response);
				            $('#latest_meeting_id').text(lastMeetingId);

				            populateHHCoverageTable(response.hhroster);

				            populateLGUCommitmentsTable(response.lgucommitments);
		                  $("#latest_lgucommitment_id").text(response.lastlgucommitmentsid);

		                  updateActivitiesTable(response);
		                  $("#latest_activity_id").text(response.lastactivityid);

		                  document.getElementById('mrid').value = response.id;
		                  document.getElementById('month').value = response.month;
		                  document.getElementById('year').value = response.year;
		                  document.getElementById('province_id').value = response.province_id;
		                  document.getElementById('mun_city_id').value = response.mun_city_id;
		                  document.getElementById('c1').value = response.hhcoverage.count_1;
		                  document.getElementById('c19').value = response.hhcoverage.count_19;
		                  document.getElementById('c24').value = response.hhcoverage.count_24;
		                  document.getElementById('tcount').value = response.hhcoverage.total_count;
				        }
				    },
				    error: function(error) {
				        console.log(error);
				    }
				});

				document.getElementById('nav-hhcoverage-tab').classList.remove('disabled');
				document.getElementById('nav-hhprofile-tab').classList.remove('disabled');
				document.getElementById('nav-macdirectory-tab').classList.remove('disabled');
				document.getElementById('nav-macmiacmeeting-tab').classList.remove('disabled');
				document.getElementById('nav-lgucommitment-tab').classList.remove('disabled');
				document.getElementById('nav-activities-tab').classList.remove('disabled');

				var tabContent = document.getElementById('nav-hhcoverage');
				var tabLink = document.getElementById('nav-hhcoverage-tab');
				tabLink.click(); 
	        });

	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-mr", function(){

	    		var currentRow = $(this).closest("tr"); 
	    		var id = currentRow.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/period/" + id,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

		        $(this).parents("tr").find("td:eq(5)").text('0');
				});

 		 // for HH Profile
	    	$('[data-toggle="tooltip"]').tooltip();
	    	var actions5 = $("#table-hhprofile td:last-child").html();    
	    	let latest_profile_id;

	    	var profileOptions = [
	    	    "PWD",
	    	    "Farmer, FW, FF",
	    	    "Solo Parent"
	    	];

	    	// Array of pwd_type options
	    	var pwdTypeOptions = [
	    	    "Psychosocial",
	    	    "Chronic Illness",
	    	    "Learning",
	    	    "Mental",
	    	    "Visual",
	    	    "Communication",
	    	    "Orthopedic"
	    	];

	    	$(document).on('change', '#hh_id', function () {
    	        var hhIdContent = $(this).val();
    	        var hhMemberDropdown = $('#hh_member');

    	        // Enable hh_member dropdown if hh_id has content
    	        if (hhIdContent !== '') {
    	        		populateHHMembersDropdown(hhIdContent);
    	            hhMemberDropdown.prop('disabled', false);
    	        } else {
    	            hhMemberDropdown.prop('disabled', true);
    	        }
    	    });

	    	$(document).on('change', '.td-content[name="hh_member"]', function() {
    	        handleHhMemberChange($(this));
    	    });

	    	// Append table with add row form on add new button click
	        $("#add-new-profile").click(function(){
	    		$(this).attr("disabled", "disabled");

	    		latest_profile_id = Number(document.getElementById("latest_profile_id").textContent) + 1;

	    		var index5 = $("#table-hhprofile tbody tr:last-child").index();
	            var row5 = '<tr>' +
	            	'<td hidden><input type="text" class="td-content" id="td_id" value="' + latest_profile_id + '"></td>' +
	            	'<td hidden><input type="text" class="td-content" name="sex" id="sex"></td>' +
	                '<td><input type="text" class="td-content" name="hh_id" id="hh_id"></td>' +
	                '<td><select class="td-content" name="hh_member" id="hh_member"></select></td>' +
	                '<td><select class="td-content" name="profile" id="profile"><option value="">Select Profile</option></select></td>' +
	                '<td><select class="td-content" name="pwd_type" id="pwd_type"><option value="">Select PWD Type</option></select></td>' +
	    			'<td>' + actions5 + '</td>' +
	            '</tr>';
	        	$("#table-hhprofile").append(row5);		
	    		$("#table-hhprofile tbody tr").eq(index5 + 1).find("#add-profile, #edit-profile").toggle();
	         $('[data-toggle="tooltip"]').tooltip();

	         $('#hh_member, #pwd_type').prop('disabled', true);
	            // Populate Profile dropdown
	            var profileDropdown = $('#profile');
	            $.each(profileOptions, function (index, value) {
	                profileDropdown.append($('<option>', {
	                    value: value,
	                    text: value
	                }));
	            });

	            // Populate pwd_type dropdown
	            var pwdTypeDropdown = $('#pwd_type');
	            $.each(pwdTypeOptions, function (index, value) {
	                pwdTypeDropdown.append($('<option>', {
	                    value: value,
	                    text: value
	                }));
	            });

	            profileDropdown.on('change', function () {
						var selectedProfile = $(this).val();
						var pwdTypeDropdown = $('#pwd_type');

						// Enable/disable pwd_type dropdown based on selected profile
						if (selectedProfile === 'PWD') {
							pwdTypeDropdown.prop('disabled', false);
						} else {
							pwdTypeDropdown.prop('disabled', true);
						}
					});
	        });

	    	// Add row on add button click
	    	$(document).on("click", "#add-profile", function(){
	    		var empty5 = false;
	    		var input5 = $(this).parents("tr").find('.td-content');

	    		let id_5;

	    		try {
	    		  id_5 = document.getElementById("hhprofid").value;
	    		}
	    		catch(err) {
	    		  id_5 = "";
	    		}

	    		let hh_id =  document.getElementById("hh_id").value;
	    		let hh_member =  document.getElementById("hh_member").value;
	    		let sex =  document.getElementById("sex").value;
	    		let profile =  document.getElementById("profile").value;
	    		let pwd_type =  document.getElementById("pwd_type").value;
	    		let prof_id = id_5.trim();

            input5.each(function(){
	    			if(!$(this).val()){
	    				if (profile !== "PWD") {
	    					$(this).removeClass("error");
	    					pwd_type = "N/A";
	    				} else{
	    					$(this).addClass("error");
	    					empty5 = true;
	    				}
	    			} else{
						$(this).removeClass("error");
					}
	    		});

	    		$(this).parents("tr").find(".error").first().focus();

	    		if(!empty5){
	    			input5.each(function(){
	    				$(this).parent("td").html($(this).val());
	    			});

					$.ajaxSetup({
					     headers: {
					           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					       }
					   });

					if(id_5 == "")
					{
						$.ajax({
				            url: "/mr/profile",
				            type:"POST",
				            data:{
								  "id":current_mrid,
								  "month":month,
								  "year":year,
								  "province_id":province_id,
								  "mun_city_id":mun_city_id,
				              "hh_id":hh_id,
				              "hh_member":hh_member,
				              "sex":sex,
				              "profile":profile,
				              "pwd_type":pwd_type			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                /*$("#ajaxform")[0].reset();*/
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });

				        document.getElementById("latest_profile_id").innerHTML = latest_profile_id;
					} else {
						$.ajax({
				            url: "/mr/profile/" + prof_id,
				            type:"PUT",
				            data:{
				              "hh_id":hh_id,
				              "hh_member":hh_member,
				              "sex":sex,
				              "profile":profile,
				              "pwd_type":pwd_type			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                $("#ajaxform")[0].reset();
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });
					}
					
	    			$(this).parents("tr").find("#add-profile, #edit-profile").toggle();
	    			$("#add-new-profile").removeAttr("disabled");
	    		}		
	        });

	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-profile", function() {
	    	    let type_5 = ["text", "text", "text", "select", "select", "select"];
	    	    let ids_5 = ["hhprofid", "sex", "hh_id", "hh_member", "profile", "pwd_type"];
	    	    let i = 0;

	    	    $(this).parents("tr").find("td:not(:last-child)").each(function() {
	    	        if (type_5[i] === "select") {
	    	            if (ids_5[i] === "profile") {
	    	                $(this).html('<select name"' + ids_5[i] + '" class="td-content" id="' + ids_5[i] + '"><option value="">Select Profile</option></select>');

	    	                var profileDropdown = $("#" + ids_5[i]);
	    	                $.each(profileOptions, function(index, value) {
	    	                    profileDropdown.append($('<option>', {
	    	                        value: value,
	    	                        text: value
	    	                    }));
	    	                });
	    	            } else if (ids_5[i] === "pwd_type") {
	    	                $(this).html('<select name="' + ids_5[i] + '" class="td-content" id="' + ids_5[i] + '"><option value="">Select PWD Type</option></select>');

	    	                var pwdTypeDropdown = $("#" + ids_5[i]);
	    	                $.each(pwdTypeOptions, function(index, value) {
	    	                    pwdTypeDropdown.append($('<option>', {
	    	                        value: value,
	    	                        text: value
	    	                    }));
	    	                });
	    	            } else if (ids_5[i] === "hh_member") {
	    	                var hhMemberDropdown = $('<select name="' + ids_5[i] + '" class="td-content" id="' + ids_5[i] + '"></select>');
	    	                $(this).html(hhMemberDropdown);

	    	                var hhIdContent = $("#hh_id").val();

	    	                if (hhIdContent !== '') {
	    	                    populateHHMembersDropdown(hhIdContent, function(populatedHHMembers) {
	    	                        $.each(populatedHHMembers, function(index, value) {
	    	                            hhMemberDropdown.append($('<option>', {
	    	                                value: value,
	    	                                text: value
	    	                            }));
	    	                        });
	    	                    });
	    	                }
	    	            } else {
	    	                $(this).html('<select name="' + ids_5[i] + '" class="td-content" id="' + ids_5[i] + '"></select>');
	    	            }
	    	        } else {
	    	            $(this).html('<input type="text" class="td-content" name="' + ids_5[i] + '"id="' + ids_5[i] + '" value="' + $(this).text() + '" disabled>');
	    	        }

	    	        i++;
	    	    });

	    	    var profileDropdown = $('#profile');

             profileDropdown.on('change', function () {
				 	var selectedProfile = $(this).val();
					var pwdTypeDropdown = $('#pwd_type');

				   // Enable/disable pwd_type dropdown based on selected profile
				   if (selectedProfile === 'PWD') {
						pwdTypeDropdown.prop('disabled', false);
					} else {
						pwdTypeDropdown.prop('disabled', true);
					}
				});

	    	    $(this).parents("tr").find("#add-profile, #edit-profile").toggle();
	    	    $("#add-new-profile").attr("disabled", "disabled");
	    	});

	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-profile", function(){

	    		var currentRow5 = $(this).closest("tr"); 
	    		var id5 = currentRow5.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/profile/" + id5,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

	            $(this).parents("tr").remove();
	    		$("#add-new-profile").removeAttr("disabled");
	        });

	    // for MAC Directory

	    	$('[data-toggle="tooltip"]').tooltip();
	    	var actions1 = $("#table-mac td:last-child").html();    
	    	let latest_mac_id;

	    	// Append table with add row form on add new button click
	        $("#add-new-mac").click(function(){
	    		$(this).attr("disabled", "disabled");

	    		latest_mac_id = Number(document.getElementById("latest_mac_id").textContent) + 1;

	    		var index1 = $("#table-mac tbody tr:last-child").index();
	            var row1 = '<tr>' +
	            	'<td hidden><input type="text" id="td_id" value="' + latest_mac_id + '"></td>' +
	                '<td><input type="text" class="form-control" name="name" id="name"></td>' +
	                '<td><input type="text" class="form-control" name="position" id="position"></td>' +
	                '<td><input type="text" class="form-control" name="contact" id="contact"></td>' +
	                '<td><input type="text" class="form-control" name="email" id="email"></td>' +
	    			'<td>' + actions1 + '</td>' +
	            '</tr>';
	        	$("#table-mac").append(row1);		
	    		$("#table-mac tbody tr").eq(index1 + 1).find("#add-mac, #edit-mac").toggle();
	            $('[data-toggle="tooltip"]').tooltip();
	        });
	    	// Add row on add button click
	    	$(document).on("click", "#add-mac", function(){
	    		var empty1 = false;
	    		var input1 = $(this).parents("tr").find('input[type="text"]');

	    		let id_1;

	    		try {
	    		  id_1 = document.getElementById("macid").value;
	    		}
	    		catch(err) {
	    		  id_1 = "";
	    		}

	    		let name =  document.getElementById("name").value;
	    		let position =  document.getElementById("position").value;
	    		let contact =  document.getElementById("contact").value;
	    		let email =  document.getElementById("email").value;
	    		let mac_id = id_1.trim();

	            input1.each(function(){
	    			if(!$(this).val()){
	    				$(this).addClass("error");
	    				empty1 = true;
	    			} else{
	                    $(this).removeClass("error");
	                }
	    		});
	    		$(this).parents("tr").find(".error").first().focus();

	    		if(!empty1){
	    			input1.each(function(){
	    				$(this).parent("td").html($(this).val());
	    			});

					$.ajaxSetup({
					     headers: {
					           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					       }
					   });

					if(id_1 == "")
					{
						$.ajax({
				            url: "/mr/mac",
				            type:"POST",
				            data:{
				              "id":current_mrid,
				              "month":month,
				              "year":year,
				              "province_id":province_id,
				              "mun_city_id":mun_city_id,
				              "name":name,
				              "position":position,
				              "contact":contact,
				              "email":email			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                /*$("#ajaxform")[0].reset();*/
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });

				        document.getElementById("latest_mac_id").innerHTML = latest_mac_id;
					} else {
						$.ajax({
				            url: "/mr/mac/" + mac_id,
				            type:"PUT",
				            data:{
				              "name":name,
				              "position":position,
				              "contact":contact,
				              "email":email			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                $("#ajaxform")[0].reset();
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });
					}
					
	    			$(this).parents("tr").find("#add-mac, #edit-mac").toggle();
	    			$("#add-new-mac").removeAttr("disabled");
	    		}		
	        });
	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-mac", function(){
	    		let ids_1 = ["macid", "name", "position", "contact", "email"];
	    		let i = 0;

	            $(this).parents("tr").find("td:not(:last-child)").each(function(){
	    			$(this).html('<input type="text" class="form-control" id = "' + ids_1[i] +'" value="' + $(this).text() + '">');
	    			i++;
	    		});	

	    		$(this).parents("tr").find("#add-mac, #edit-mac").toggle();
	    		$("#add-new-mac").attr("disabled", "disabled");
	        });
	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-mac", function(){

	    		var currentRow1 = $(this).closest("tr"); 
	    		var id1 = currentRow1.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/mac/" + id1,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

	            $(this).parents("tr").remove();
	    		$("#add-new-mac").removeAttr("disabled");
	        });

	    // for MAC Meeting
	    	$('[data-toggle="tooltip"]').tooltip();  
	    	let latest_meeting_id;


	    	$("#add-new-meeting").click(function(){
    	        // ... your existing code ...
	    		  $(this).attr("disabled", "disabled");
	    		  var index2 = $("#table-meeting tbody tr:last-child").index();
	    		  latest_meeting_id = Number(document.getElementById("latest_meeting_id").textContent) + 1;

    	        var template = $("#hidden-row-template").html();
    	        var row2 = $(template);

    	        row2.find("#td_id").val(latest_meeting_id);

    	        $("#table-meeting").append(row2);
    	        $("#table-meeting tbody tr").eq(index2 + 1).find("#add-meeting, #edit-meeting").toggle();
    	    });

    	    $('[data-toggle="tooltip"]').tooltip();

	    	$(document).on("click", "#add-meeting", function(){
	    		var empty2 = false;
	    		var input2 = $(this).parents("tr").find('.td-content');

	    		let id_2;

	    		try {
	    		  id_2 = document.getElementById("mmid").value;
	    		}
	    		catch(err) {
	    		  id_2 = "";
	    		}

	    		let date_conducted =  document.getElementById("date_conducted").value;
	    		let venue =  document.getElementById("venue").value;
	    		let agenda =  document.getElementById("agenda").value;
	    		let resolution =  document.getElementById("resolution").value;
	    		let date_sub_mom =  document.getElementById("date_sub_mom").value;
	    		let meeting_id = id_2.trim();

	            input2.each(function(){
	    			if(!$(this).val()){
	    				$(this).addClass("error");
	    				empty2 = true;
	    			} else{
	                    $(this).removeClass("error");
	                }
	    		});
	    		$(this).parents("tr").find(".error").first().focus();

	    		if(!empty2){
	    			input2.each(function(){
	    				$(this).parent("td").html($(this).val());
	    			});

					$.ajaxSetup({
					     headers: {
					           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					       }
					   });

					if(id_2 == "")
					{
						$.ajax({
				            url: "/mr/meeting",
				            type:"POST",
				            data:{
				              "id":current_mrid,
				              "month":month,
				              "year":year,
				              "province_id":province_id,
				              "mun_city_id":mun_city_id,
				              "date_conducted":date_conducted,
				              "venue":venue,
				              "agenda":agenda,
				              "resolution":resolution,
				              "date_sub_mom":date_sub_mom			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                /*$("#ajaxform")[0].reset();*/
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });

				        document.getElementById("latest_meeting_id").innerHTML = latest_meeting_id;
					} else {
						$.ajax({
				            url: "/mr/meeting/" + meeting_id,
				            type:"PUT",
				            data:{
				              "date_conducted":date_conducted,
				              "venue":venue,
				              "agenda":agenda,
				              "resolution":resolution,
				              "date_sub_mom":date_sub_mom			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                $("#ajaxform")[0].reset();
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });
					}
					
	    			$(this).parents("tr").find("#add-meeting, #edit-meeting").toggle();
	    			$("#add-new-meeting").removeAttr("disabled");
	    		}		
	        });
	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-meeting", function(){
	    		let type_2 = ["text", "date", "text", "textarea", "textarea", "date"];
	    		let ids_2 = ["mmid", "date_conducted", "venue", "agenda", "resolution", "date_sub_mom"];
	    		let j = 0;

	            $(this).parents("tr").find("td:not(:last-child)").each(function(){

	            if(type_2[j] == "textarea"){
	            	$(this).html('<textarea rows="5" class="form-control td-content" id = "' + ids_2[j] +'">' + $(this).text() + '</textarea>');
	            } else {
	            	$(this).html('<input type="' + type_2[j] + '" class="form-control td-content" id = "' + ids_2[j] +'" value="' + $(this).text().trim() + '">');
	            }
	    			
	    			j++;
	    		});	

	    		$(this).parents("tr").find("#add-meeting, #edit-meeting").toggle();
	    		$("#add-new-meeting").attr("disabled", "disabled");
	        });
	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-meeting", function(){

	    		var currentRow2 = $(this).closest("tr"); 
	    		var id2 = currentRow2.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/meeting/" + id2,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

	            $(this).parents("tr").remove();
	    		$("#add-new-meeting").removeAttr("disabled");
	        });

	    // for LGU Commitment Report
	    	$('[data-toggle="tooltip"]').tooltip(); 
	    	let latest_lgucommitment_id;

	    	// Append table with add row form on add new button click
	        $("#add-new-lgucommitment").click(function(){
	            $(this).attr("disabled", "disabled");

	            // Get the latest_lgucommitment_id and increment it
	            var latest_lgucommitment_id = Number($("#latest_lgucommitment_id").text()) + 1;

	            var index3 = $("#table-lgucommitment tbody tr:last-child").index();
	            var actions3 = '<a class="add" id="add-lgucommitment" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a><a class="edit" id="edit-lgucommitment" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a class="delete" id="delete-lgucommitment" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>';
	            var row3 = '<tr>' +
	              '<td hidden><input type="text" class="td-content" id="td_id" value="' + latest_lgucommitment_id + '"></td>' +
	              '<td><input type="text" class="form-control td-content" name="barangay" id="barangay"></td>' +
	              '<td><textarea rows="5" class="form-control td-content" name="ssa_gap" id="ssa_gap"></textarea></td>' +
	              '<td><textarea rows="5" class="form-control td-content" name="commitment" id="commitment"></textarea></td>' +
	              '<td><input type="text" class="form-control td-content" name="timeline" id="timeline"></td>' +
	              '<td><input type="text" class="form-control td-content" name="budget_source" id="budget_source"></td>' +
	              '<td><select name="status" class="td-content" id="status"><option value="Ongoing">Ongoing</option><option value="Completed">Completed</option><option value="Cancelled">Cancelled</option></select></td>' +
	              '<td><textarea rows="5" class="form-control td-content" name="remarks" id="remarks"></textarea></td>' +
	              '<td>' + actions3 + '</td>' +
	            '</tr>';
	            $("#table-lgucommitment").append(row3);
	            $("#table-lgucommitment tbody tr").eq(index3 + 1).find("#add-lgucommitment, #edit-lgucommitment").toggle();
	            $('[data-toggle="tooltip"]').tooltip();
	          });
	    	// Add row on add button click
	    	$(document).on("click", "#add-lgucommitment", function(){
	    		var empty3 = false;
	    		var input3 = $(this).parents("tr").find('.td-content');

	    		let id_3;

	    		try {
	    		  id_3 = document.getElementById("lgucid").value;
	    		}
	    		catch(err) {
	    		  id_3 = "";
	    		}

	    		let barangay =  document.getElementById("barangay").value;
	    		let ssa_gap =  document.getElementById("ssa_gap").value;
	    		let commitment =  document.getElementById("commitment").value;
	    		let timeline =  document.getElementById("timeline").value;
	    		let budget_source =  document.getElementById("budget_source").value;
	    		let status =  document.getElementById("status").value;
	    		let remarks =  document.getElementById("remarks").value;
	    		let lgucommitment_id = id_3.trim();

	            input3.each(function(){
	    			if(!$(this).val()){
	    				$(this).addClass("error");
	    				empty3 = true;
	    			} else{
	                    $(this).removeClass("error");
	                }
	    		});
	    		$(this).parents("tr").find(".error").first().focus();

	    		if(!empty3){
	    			input3.each(function(){
	    				$(this).parent("td").html($(this).val());
	    			});

					$.ajaxSetup({
					     headers: {
					           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					       }
					   });

					if(id_3 == "")
					{
						$.ajax({
				            url: "/mr/lgucommitment",
				            type:"POST",
				            data:{
				              "id":current_mrid,
				              "month":month,
				              "year":year,
				              "province_id":province_id,
				              "mun_city_id":mun_city_id,
				              "barangay":barangay,
				              "ssa_gap":ssa_gap,
				              "commitment":commitment,
				              "timeline":timeline,
				              "budget_source":budget_source,
				              "status":status,
				              "remarks":remarks			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                /*$("#ajaxform")[0].reset();*/
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });

				        document.getElementById("latest_lgucommitment_id").innerHTML = latest_lgucommitment_id;
					} else {
						$.ajax({
				            url: "/mr/lgucommitment/" + lgucommitment_id,
				            type:"PUT",
				            data:{
				              "barangay":barangay,
				              "ssa_gap":ssa_gap,
				              "commitment":commitment,
				              "timeline":timeline,
				              "budget_source":budget_source,
				              "status":status,
				              "remarks":remarks				
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                $("#ajaxform")[0].reset();
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });
					}
					
	    			$(this).parents("tr").find("#add-lgucommitment, #edit-lgucommitment").toggle();
	    			$("#add-new-lgucommitment").removeAttr("disabled");
	    		}		
	        });
	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-lgucommitment", function(){
	    		let type_3 = ["text", "text", "textarea", "textarea", "text", "text", "select", "textarea"];
	    		let ids_3 = ["lgucid", "barangay", "ssa_gap", "commitment", "timeline", "budget_source", "status", "remarks"];
	    		let k = 0;

	            $(this).parents("tr").find("td:not(:last-child)").each(function(){

	            if(type_3[k] == "textarea"){
	            	$(this).html('<textarea rows="5" class="form-control td-content" id = "' + ids_3[k] +'">' + $(this).text() + '</textarea>');
	            } else if(type_3[k] == "select"){
	            	$(this).html('<select name="status" class="td-content" id="' + ids_3[k] +'"><option value="Ongoing">Ongoing</option><option value="Completed">Completed</option><option value="Cancelled">Cancelled</option></select>');
	            } else {
	            	$(this).html('<input type="' + type_3[k] + '" class="form-control td-content" id = "' + ids_3[k] +'" value="' + $(this).text().trim() + '">');
	            }

	    			k++;
	    		});	

	    		$(this).parents("tr").find("#add-lgucommitment, #edit-lgucommitment").toggle();
	    		$("#add-new-lgucommitment").attr("disabled", "disabled");
	        });
	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-lgucommitment", function(){

	    		var currentRow3 = $(this).closest("tr"); 
	    		var id3 = currentRow3.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/lgucommitment/" + id3,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

	            $(this).parents("tr").remove();
	    		$("#add-new-lgucommitment").removeAttr("disabled");
	        });

	    // for Activities   	
	    	$('[data-toggle="tooltip"]').tooltip();
	    	let latest_activity_id;

	    	// Append table with add row form on add new button click
	        $("#add-new-activity").click(function(){
	    		$(this).attr("disabled", "disabled");

	    		    var latest_activity_id = Number(document.getElementById("latest_activity_id").textContent) + 1;

	    		    var actions4 = '<a class="add" id="add-activities" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>' +
	    		                   '<a class="edit" id="edit-activities" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>' +
	    		                   '<a class="delete" id="delete-activities" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>';

	    		    var index4 = $("#table-activities tbody tr:last-child").index();
	    		    var row4 = '<tr>' +
	    		                '<td hidden><input type="text" class="td-content" id="td_id" value="' + latest_activity_id + '"></td>' +
	    		                '<td><select name="category" class="td-content" id="category"><option value="Program">Program Related</option><option value="Other Program">Other Program</option><option value="LGU">LGU Activity</option></select></td>' +
	    		                '<td><input type="date" class="form-control td-content" name="date_conducted" id="date_conducted"></td>' +
	    		                '<td><textarea rows="5" class="form-control td-content" name="title" id="title"></textarea></td>' +
	    		                '<td><textarea rows="5" class="form-control td-content" name="description" id="description"></textarea></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="venue" id="venue"></td>' +
	    		                '<td><textarea rows="5" class="form-control td-content" name="objective" id="objective"></textarea></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="male" id="male"></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="female" id="female"></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="ip" id="ip"></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="fund_source" id="fund_source"></td>' +
	    		                '<td><input type="text" class="form-control td-content" name="amount" id="amount"></td>' +
	    		                '<td>' + actions4 + '</td>' +
	    		            '</tr>';
	    		    $("#table-activities tbody").append(row4);
	    		    $("#table-activities tbody tr").eq(index4 + 1).find("#add-activities, #edit-activities").toggle();
	    		    $('[data-toggle="tooltip"]').tooltip();
	        });
	    	// Add row on add button click
	    	$(document).on("click", "#add-activities", function(){
	    		var empty4 = false;
	    		var input4 = $(this).parents("tr").find('.td-content');

	    		let id_4;

	    		try {
	    		  id_4 = document.getElementById("actid").value;
	    		}
	    		catch(err) {
	    		  id_4 = "";
	    		}

	    		let category = document.getElementById("category").value;
	    		let date_conducted = document.getElementById("date_conducted").value;
	    		let title = document.getElementById("title").value;
	    		let description = document.getElementById("description").value;
	    		let venue = document.getElementById("venue").value;
	    		let objective = document.getElementById("objective").value;
	    		let male = document.getElementById("male").value;
	    		let female = document.getElementById("female").value;
	    		let ip = document.getElementById("ip").value;
	    		let fund_source = document.getElementById("fund_source").value;
	    		let amount = document.getElementById("amount").value;
	    		let activity_id = id_4.trim();

	            input4.each(function(){
	    			if(!$(this).val()){
	    				$(this).addClass("error");
	    				empty4 = true;
	    			} else{
	                    $(this).removeClass("error");
	                }
	    		});
	    		$(this).parents("tr").find(".error").first().focus();

	    		if(!empty4){
	    			input4.each(function(){
	    				$(this).parent("td").html($(this).val());
	    			});

					$.ajaxSetup({
					     headers: {
					           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					       }
					   });

					if(id_4 == "")
					{
						$.ajax({
				            url: "/mr/activities",
				            type:"POST",
				            data:{
				              "id":current_mrid,
				              "month":month,
				              "year":year,
				              "province_id":province_id,
				              "mun_city_id":mun_city_id,
				              "category":category,
				              "date_conducted":date_conducted,
				              "title":title,
				              "description":description,
				              "venue":venue,
				              "objective":objective,
				              "male":male,
				              "female":female,
				              "ip":ip,
				              "fund_source":fund_source,
				              "amount":amount			
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                /*$("#ajaxform")[0].reset();*/
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });

				        document.getElementById("latest_activity_id").innerHTML = latest_activity_id;
					} else {
						$.ajax({
				            url: "/mr/activities/" + activity_id,
				            type:"PUT",
				            data:{
				              "category":category,
				              "date_conducted":date_conducted,
				              "title":title,
				              "description":description,
				              "venue":venue,
				              "objective":objective,
				              "male":male,
				              "female":female,
				              "ip":ip,
				              "fund_source":fund_source,
				              "amount":amount				
				            },
				            success:function(response){
				              console.log(response);
				              if(response) {
				                $('.success').text(response.success);
				                $("#ajaxform")[0].reset();
				              }
				            },
				            error: function(error) {
				             console.log(error);
				            }
				        });
					}
					
	    			$(this).parents("tr").find("#add-activities, #edit-activities").toggle();
	    			$("#add-new-activity").removeAttr("disabled");
	    		}		
	        });
	    	// Edit row on edit button click
	    	$(document).on("click", "#edit-activities", function(){
	    		let type_4 = ["text", "select", "date", "textarea", "textarea", "text", "textarea", "text", "text", "text", "text", "text"];
	    		let ids_4 = ["actid", "category", "date_conducted", "title", "description", "venue", "objective", "male", "female", "ip", "fund_source", "amount"];
	    		let l = 0;

	            $(this).parents("tr").find("td:not(:last-child)").each(function(){

	            if(type_4[l] == "textarea"){
	            	$(this).html('<textarea rows="5" class="form-control td-content" id = "' + ids_4[l] +'">' + $(this).text() + '</textarea>');
	            } else if(type_4[l] == "select"){
	            	$(this).html('<select name="category" class="td-content" id="' + ids_4[l] +'"><option value="Program">Program Related</option><option value="Other Program">Other Program</option><option value="LGU">LGU Activity</option></select>');
	            }else{
	            	$(this).html('<input type="' + type_4[l] + '" class="form-control td-content" id = "' + ids_4[l] +'" value="' + $(this).text().trim() + '">');
	            }

	    			l++;
	    		});	

	    		$(this).parents("tr").find("#add-activities, #edit-activities").toggle();
	    		$("#add-new-activity").attr("disabled", "disabled");
	        });
	    	// Delete row on delete button click
	    	$(document).on("click", "#delete-activities", function(){

	    		var currentRow4 = $(this).closest("tr"); 
	    		var id4 = currentRow4.find("td:eq(0)").text();

	    		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
	    		});

				$.ajax({
		            url: "/mr/activities/" + id4,
		            type:"PATCH",
		            success:function(response){
		              console.log(response);
		              if(response) {
		                $('.success').text(response.success);
		                $("#ajaxform")[0].reset();
		              }
		            },
		            error: function(error) {
		             console.log(error);
		            }
		        });

	            $(this).parents("tr").remove();
	    		$("#add-new-activity").removeAttr("disabled");
	        });

	    });

		/*For the muncity dropdown*/
		function updateMuncityDropdown() {
		     var provinceDropdown = document.getElementById('mr-province');
		     var muncityDropdown = document.getElementById('mr-muncity');
		     var selectedProvince = provinceDropdown.value;

		     // Clear existing options
		     muncityDropdown.innerHTML = '<option value="" selected>Select Municipality</option>';

		     // Get the municipalities for the selected province
		     var muncities = @json($provincesAndMuncities);

		     if (selectedProvince && muncities[selectedProvince]) {
		         muncities[selectedProvince].forEach(function(muncity) {
		             var option = document.createElement('option');
		             option.value = muncity.muncity;
		             option.textContent = muncity.muncity;
		             muncityDropdown.appendChild(option);
		         });
		     }
		 }
		
		/*Save HH Coverage without reloading the page*/
		document.getElementById("hhCoverageForm").addEventListener("submit", function (event) {
	        event.preventDefault(); // Prevent the default form submission

	        var formData = new FormData(this);

	        fetch(this.action, {
	            method: "POST",
	            body: formData,
	        })
            .then(function (response) {
                if (response.status === 200) {
                    alert("HH Coverage saved successfully!");
                } else {
                    alert("HH Coverage submission failed.");
                }
            })
            .catch(function (error) {
                alert("An error occurred while submitting the form.");
            })
            .finally(function () {
            });
	    });

		/*Functions*/	

		 //For the HH Profile Dropdowns
		 function populateHHMembersDropdown(hhId) {
		     if (hhId !== '') {
		         $.ajax({
		             url: '/mr/profile/' + hhId, 
		             method: 'GET',
		             success: function (data) {
		                 $('#hh_member').empty(); 
		                 
		                 $.each(data, function (key, value) {
		                     $('#hh_member').append($('<option>', {
		                         value: value,
		                         text: value
		                     }));
		                 });
		             }
		         });
		     }
		 }

		 function handleHhMemberChange(selectElement) {
		     const selectedOption = selectElement.val();
		     const sexInput = selectElement.closest("tr").find(".td-content[name='sex']");

		     // Make an AJAX request to fetch data from the database
		     $.ajax({
		         url: "/mr/member/", // Replace with your server-side script
		         method: "GET",
		         data: { fullname: selectedOption },
		         success: function(data) {
		             // Update the sex input field with fetched data
		             sexInput.val(data);
		         },
		         error: function() {
		             console.error("Error fetching data from the database.");
		         }
		     });
		 }

		 function populateHHCoverageTable(hhroster) {
		     var tableBody = $('#table-hhcoverage tbody');

		     tableBody.empty(); // Clear existing rows
		     
		     if (hhroster.length > 0) {
		         // Loop through the fetched values and populate the table
		         $.each(hhroster, function(index, roster) {
		             var newRow = `
		                 <tr>
		                     <td>${roster.barangay}</td>
		                     <td>${roster.hh_count}</td>
		                     <td>${roster.entry_count}</td>
		                 </tr>
		             `;
		             tableBody.append(newRow);
		         });
		     } else {
		         var emptyRow = `
		             <tr hidden>
		                 <td></td>
		                 <td></td>
		                 <td></td>
		             </tr>
		         `;
		         tableBody.append(emptyRow);
		     }
		 }

		 function populateMeetingTable(meeting) {
		     var tableBody = $('#table-meeting tbody');

		     tableBody.empty(); // Clear existing rows
		     
		     if (meeting && meeting.macmeetings.length > 0) {
		         // Loop through the fetched values and populate the table
		         $.each(meeting.macmeetings, function(index, macmeeting) {
		             var newRow = `
		                 <tr>
		                     <td hidden>${macmeeting.id}</td>
		                     <td>${macmeeting.date_conducted}</td>
		                     <td>${macmeeting.venue}</td>
		                     <td>${macmeeting.agenda}</td>
		                     <td>${macmeeting.resolution}</td>
		                     <td>${macmeeting.date_sub_mom}</td>
		                     <td>
		                         <a class="add" id="add-meeting" title="Add" data-toggle="tooltip">
		                             <i class="material-icons">&#xE03B;</i>
		                         </a>
		                         <a class="edit" id="edit-meeting" title="Edit" data-toggle="tooltip">
		                             <i class="material-icons">&#xE254;</i>
		                         </a>
		                         <a class="delete" id="delete-meeting" title="Delete" data-toggle="tooltip">
		                             <i class="material-icons">&#xE872;</i>
		                         </a>
		                     </td>
		                 </tr>
		             `;
		             tableBody.append(newRow);
		         });
		     } else {
		         var emptyRow = `
		             <tr hidden>
		                 <td></td>
		                 <td></td>
		                 <td></td>
		                 <td></td>
		                 <td></td>
		                 <td></td>
		                 <td>
		                     <a class="add" id="add-meeting" title="Add" data-toggle="tooltip">
		                         <i class="material-icons">&#xE03B;</i>
		                     </a>
		                     <a class="edit" id="edit-meeting" title="Edit" data-toggle="tooltip">
		                         <i class="material-icons">&#xE254;</i>
		                     </a>
		                     <a class="delete" id="delete-meeting" title="Delete" data-toggle="tooltip">
		                         <i class="material-icons">&#xE872;</i>
		                     </a>
		                 </td>
		             </tr>
		         `;
		         tableBody.append(emptyRow);
		     }
		 }

		function populateLGUCommitmentsTable(lgucommitments) {
		  // Empty the current table rows
		  $("#table-lgucommitment tbody").empty();

		  if (lgucommitments.length > 0) {
		     // Add the LGU commitments
		     lgucommitments.forEach(function(commitment) {
		       $("#table-lgucommitment tbody").append('<tr><td hidden>' + commitment.id + '</td><td>' + commitment.barangay + '</td><td>' + commitment.ssa_gap + '</td><td>' + commitment.commitment + '</td><td>' + commitment.timeline + '</td><td>' + commitment.budget_source + '</td><td>' + commitment.status + '</td><td>' + commitment.remarks + '</td><td><a class="add" id="add-lgucommitment" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a><a class="edit" id="edit-lgucommitment" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a class="delete" id="delete-lgucommitment" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td></tr>');
		     });
		   } else {
		     // Add a hidden row for empty LGU commitments
		     $("#table-lgucommitment tbody").append('<tr hidden><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><a class="add" id="add-lgucommitment" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a><a class="edit" id="edit-lgucommitment" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a class="delete" id="delete-lgucommitment" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td></tr>');
		   }
		}

		function updateActivitiesTable(activities) {
		    var tableBody = $('#table-activities tbody');
		    tableBody.empty(); // Clear existing rows

		    if (activities && activities.activities.length > 0) {
		        // Loop through the fetched values and populate the table
		        $.each(activities.activities, function(index, activity) {
		            var newRow = `
		                <tr>
		                    <td hidden>${activity.id}</td>
		                    <td>${activity.category}</td>
		                    <td>${activity.date_conducted}</td>
		                    <td>${activity.title}</td>
		                    <td>${activity.description}</td>
		                    <td>${activity.venue}</td>
		                    <td>${activity.objective}</td>
		                    <td>${activity.male}</td>
		                    <td>${activity.female}</td>
		                    <td>${activity.ip}</td>
		                    <td>${activity.fund_source}</td>
		                    <td>${activity.amount}</td>
		                    <td>
		                        <a class="add" id="add-activities" title="Add" data-toggle="tooltip">
		                            <i class="material-icons">&#xE03B;</i>
		                        </a>
		                        <a class="edit" id="edit-activities" title="Edit" data-toggle="tooltip">
		                            <i class="material-icons">&#xE254;</i>
		                        </a>
		                        <a class="delete" id="delete-activities" title="Delete" data-toggle="tooltip">
		                            <i class="material-icons">&#xE872;</i>
		                        </a>
		                    </td>
		                </tr>
		            `;
		            tableBody.append(newRow);
		        });
		    } else {
		        var emptyRow = `
		            <tr hidden>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td>
		                    <a class="add" id="add-activities" title="Add" data-toggle="tooltip">
		                        <i class="material-icons">&#xE03B;</i>
		                    </a>
		                    <a class="edit" id="edit-activities" title="Edit" data-toggle="tooltip">
		                        <i class="material-icons">&#xE254;</i>
		                    </a>
		                    <a class="delete" id="delete-activities" title="Delete" data-toggle="tooltip">
		                        <i class="material-icons">&#xE872;</i>
		                    </a>
		                </td>
		            </tr>
		        `;
		        tableBody.append(emptyRow);
		    }
		}

   </script>

   <script id="hidden-row-template" type="text/template">
       <tr>
           <td hidden><input type="text" class="td-content" id="td_id" value=""></td>
           <td><input type="date" class="form-control td-content" name="date_conducted" id="date_conducted"></td>
           <td><input type="text" class="form-control td-content" name="venue" id="venue"></td>
           <td><textarea rows="5" class="form-control td-content" name="agenda" id="agenda"></textarea></td>
           <td><textarea rows="5" class="form-control td-content" name="resolution" id="resolution"></textarea></td>
           <td><input type="date" class="form-control td-content" name="date_sub_mom" id="date_sub_mom"></td>
           <td>
               <a class="add" id="add-meeting" title="Add" data-toggle="tooltip">
                   <i class="material-icons">&#xE03B;</i>
               </a>
               <a class="edit" id="edit-meeting" title="Edit" data-toggle="tooltip">
                   <i class="material-icons">&#xE254;</i>
               </a>
               <a class="delete" id="delete-meeting" title="Delete" data-toggle="tooltip">
                   <i class="material-icons">&#xE872;</i>
               </a>
           </td>
       </tr>
   </script>
@endpush