@extends('layouts.app')

@section('stylesheets')

    <style>
        .export-container {
            background-color: #fff !important;
            border: 1px solid #ccc;
            padding: 15px;
        }

        .mr-container1 .input-group-text {
            background-color: #555;
            color: #fff;
        }

        .mr-container1 select.form-control {
            border: 1px solid #555;
            color: #555;
        }

        .mr-container1 select.form-control:focus {
            border-color: #555;
            color: #555;
        }

        .export-mr {
            margin-top: 20px;
        }

        /* CSS for Upload Button */
        .export-mr .generate.add-new {
            background-color: #555 !important;
            color: #fff !important; 
            border: none;
            border-radius: 4px;
            padding: 5px 5px;
            font-weight: bold;
            transition: background-color 0.3s, color 0.3s;
        }

        .export-mr .generate.add-new:not([disabled]):hover {
            background-color: #fff !important; 
            color: #555 !important;
            border-style: solid !important;
            border-color: #555 !important;
        }
   
    </style>
@endsection

@section('content')
  <div class="export-container">
    <div class="mr-container1">
      <form method="POST" action="" class="mt-3">
        <div class="my-3 row">
          <div class="col-md-2">
            <div class="input-group">
              <span class="input-group-text">Province:</span>
              <select aria-label="province" name="mr-province" id="mr-province" class="form-control" onchange="updateMuncityDropdown()">
                <option value="" selected>Select Province</option>
                @foreach($provincesAndMuncities as $province => $muncities)
                  <option value="{{ $province }}">{{ $province }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="input-group">
              <span class="input-group-text">Municipality:</span>
              <select aria-label="municipality" name="mr-muncity" id="mr-muncity" class="form-control">
                <option value="" selected>Select Municipality</option>
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group">
              <span class="input-group-text">From:</span>
              <input type="month" aria-label="date-month" name="date-month" id="date-month" class="form-control" value="">
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group">
              <span class="input-group-text">To:</span>
              <input type="month" aria-label="date-to" name="date-to" id="date-to" class="form-control" value="">
            </div>
          </div>
          <div class="col-md-3">
            <div class="input-group">
              <span class="input-group-text">Type of Report:</span>
              <select aria-label="Report Type" name="report-type" id="report-type" class="form-control">
                <option value="" selected>Select Report Type</option>
                <option value="hh-coverage">Household Coverage</option>
                <option value="hh-profile">Household Profile</option>
              </select>
            </div>
          </div>
        </div>
      </form>
    </div>

    <div class="export-mr" style="border: 1px solid #ccc; padding: 15px; border-radius: 5px;">
      <h2>Export Report</h2>
      <form id="excel-generation-form" method="get" enctype="multipart/form-data">
        @csrf
        <input type="hidden" id="hidden-muncity" value="">
        <input type="hidden" id="hidden-month" value="">
        <input type="hidden" id="hidden-to" value="">
        <fieldset>
          <label style="font-weight: bold;">Export Excel:</label>
          <button type="submit" id="generate-excel-btn" class="generate add-new" disabled>Generate Report</button>
          <p id="warning-message" style="color: red; display: none;">Please select a muncity and a month.</p>
        </fieldset>
      </form>
    </div>
  </div>
@endsection

@push('scripts')
    <script>
        // For the date picker
        // Get the "date-month" and "date-to" input elements
          const dateFromInput = document.getElementById('date-month');
          const dateToInput = document.getElementById('date-to');

          // Get the current date
          const currentDate = new Date();
          const currentYear = currentDate.getFullYear();
          const currentMonth = currentDate.getMonth() + 1; // Months are 0-based

          // Set the min and max attributes of the "date-from" input
          dateFromInput.setAttribute('max', `${currentYear}-${currentMonth.toString().padStart(2, '0')}`);

          // Add an event listener to "date-month" input to update "date-to" input
          dateFromInput.addEventListener('input', updateDateToOptions);

          function updateDateToOptions() {
            const selectedDate = dateFromInput.value;

            if (selectedDate) {
              // If "date-from" has a selected value, enable "date-to" and update its options
              /*dateToInput.disabled = false;
*/
              const selectedDateObj = new Date(selectedDate);
              const selectedYear = selectedDateObj.getFullYear();
              const selectedMonth = selectedDateObj.getMonth() + 1;

              // Set the min and max attributes of the "date-to" input based on the selected date and the current date
              dateToInput.setAttribute('min', formatDate(selectedDateObj));
              dateToInput.setAttribute('max', `${currentYear}-${currentMonth.toString().padStart(2, '0')}`);
            } else {
              // If "date-from" has no selected value, disable "date-to" and clear its value
              /*dateToInput.disabled = true;*/
              dateToInput.value = '';
            }
          }

          function formatDate(date) {
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            return `${year}-${month.toString().padStart(2, '0')}`;
          }

          // Initialize "date-to" options based on the current date
          updateDateToOptions();


        // Update the municipality dropdown when the province dropdown changes
        const updateMuncityDropdown = () => {
          const provinceDropdown = document.getElementById('mr-province');
          const muncityDropdown = document.getElementById('mr-muncity');
          const selectedProvince = provinceDropdown.value;

          // Clear the existing municipality options
          muncityDropdown.innerHTML = '<option value="" selected>Select Municipality</option>';

          // Get the municipalities for the selected province
          const muncities = @json($provincesAndMuncities);

          if (selectedProvince && muncities[selectedProvince]) {
            // Add the municipalities to the dropdown
            muncities[selectedProvince].forEach((muncity) => {
              const option = document.createElement('option');
              option.value = muncity.muncity;
              option.textContent = muncity.muncity;
              muncityDropdown.appendChild(option);
            });
          }

          // Update the action of the excel generation form
          updatePdfFormAction();
        };

        // Update the action of the excel generation form when the municipality dropdown or month input changes
        const updatePdfFormAction = () => {
          const selectedMProvince = document.getElementById('mr-province').value;
          const selectedMuncity = document.getElementById('mr-muncity').value;
          const selectedMonth = document.getElementById('date-month').value;
          const selectedTo = document.getElementById('date-to').value;
          const selectedReport = document.getElementById('report-type').value;
          const excelForm = document.getElementById('excel-generation-form');
          const hiddenMuncityInput = document.getElementById('hidden-muncity');
          const hiddenMonthInput = document.getElementById('hidden-month');
          const hiddenToInput = document.getElementById('hidden-to');
          const generateExcelButton = document.getElementById('generate-excel-btn');

          // Enable the generate excel button only if the required fields are selected
          generateExcelButton.disabled = !selectedMProvince || !selectedMuncity || !selectedMonth || !selectedReport;
    
          if (selectedReport === 'hh-coverage') {
              excelForm.action = `/generateHHCoverage/${selectedMProvince}/${selectedMuncity}/${selectedMonth}/${selectedTo}`;
              hiddenMuncityInput.value = selectedMuncity;
              hiddenMonthInput.value = selectedMonth;
              hiddenToInput.value = selectedTo;
              generateExcelButton.disabled = false;
            } else if (selectedReport === 'hh-profile') {
              excelForm.action = `/generateHHProfile/${selectedMProvince}/${selectedMuncity}/${selectedMonth}/${selectedTo}`;
              hiddenMuncityInput.value = selectedMuncity;
              hiddenMonthInput.value = selectedMonth;
              hiddenToInput.value = selectedTo;
              generateExcelButton.disabled = false;
            } else {
              excelForm.action = '';
              hiddenMuncityInput.value = '';
              hiddenMonthInput.value = '';
              generateExcelButton.disabled = true;
            }
        };

        // Call the update functions when the page loads
        window.addEventListener('load', () => {
          updateMuncityDropdown();
          updatePdfFormAction();
        });

        // Call the update functions when the municipality dropdown or month input changes
        document.getElementById('mr-muncity').addEventListener('change', updatePdfFormAction);
        document.getElementById('date-month').addEventListener('change', updatePdfFormAction);
        document.getElementById('date-to').addEventListener('change', updatePdfFormAction);
        document.getElementById('report-type').addEventListener('change', updatePdfFormAction);

        // Prevent the default form submission behavior and add a loading spinner to the button
        document.getElementById('generate-excel-btn').addEventListener('click', (event) => {
          event.preventDefault();

          // Submit the form
          document.getElementById('excel-generation-form').submit();
        });

    </script>
 @endpush