<!-- activity_table.blade.php -->
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Date Conducted</th>
            <th>Title</th>
            <th>Venue</th>
            <th>Objective</th>
            <th>Male Participants</th>
            <th>Female Participants</th>
            <th>Total Participants</th>
            <th>IP Participants</th>
            <th>Fund Source</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($activityData as $index => $entry)
            <tr>
                <td>{{$index + 1}}</td>
                <td>{{$entry->date_conducted}}</td>
                <td>{{$entry->title}}</td>
                <td>{{$entry->venue}}</td>
                <td>{{$entry->objective}}</td>
                <td>{{$entry->male}}</td>
                <td>{{$entry->female}}</td>
                <td>{{$entry->total}}</td>
                <td>{{$entry->ip}}</td>
                <td>{{$entry->fund_source}}</td>
                <td>{{$entry->amount}}</td>
            </tr>
        @endforeach
            <tr>
                <td colspan="11">Brief Description of the Activity</td>
            </tr>
        @foreach ($activityData as $index => $entry)
            <tr>
                <td>{{$index + 1}}</td>
                <td colspan="10">{{$entry->description}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
