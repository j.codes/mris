<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sidebar</title>
    <!-- Include Font Awesome for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <style>
       
    </style>
</head>
<body>
    <div class="sidebar-body">
        <div class="row">
            <div class="sidebar-main d-flex flex-column flex-shrink-0 p-3 bg-body-tertiary">
                <div class="branding">
                    <img src="{{ asset('storage/images/4ps.png') }}" alt="Logo">
                    <h3>MRIS</h3>
                </div>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="/home" class="nav-link" aria-current="page">
                            <i class="fas fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard" class="nav-link">
                            <i class="fas fa-chart-bar"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/mr" class="nav-link">
                            <i class="fas fa-table"></i>
                            <span>Monthly Report</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/import" class="nav-link">
                            <i class="fas fa-upload"></i>
                            <span>Import Data</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/export" class="nav-link">
                            <i class="fas fa-download"></i>
                            <span>Export Data</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/report" class="nav-link">
                            <i class="fas fa-th"></i>
                            <span>Generate Report</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/account" class="nav-link">
                            <i class="fas fa-user-circle"></i>
                            <span>Account</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
