@extends('layouts.app')

@section('stylesheets')
<style>
    /* Custom CSS for Send Password Reset Link Form */
    .reset-card {
        border: 1px solid #ccc;
        padding: 15px;
        border-radius: 5px;
    }

    .reset-card-header {
        color: #555;
    }

    .reset-alert {
        background-color: #dff0d8;
        color: #3c763d;
        border: 1px solid #3c763d;
        border-radius: 4px;
        padding: 10px;
        margin-bottom: 20px;
    }

    .reset-button {
        background-color: #555 !important;
        color: #fff !important;
        border: none;
        border-radius: 4px;
        padding: 5px 10px;
        font-weight: bold;
        transition: background-color 0.3s, color 0.3s;
    }

    .reset-button:hover {
        background-color: #fff !important;
        color: #555 !important;
        border-style: solid !important;
        border-color: #555 !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card reset-card">
                <div class="card-header reset-card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn reset-button">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
