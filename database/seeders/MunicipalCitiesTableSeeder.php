<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MunicipalCitiesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
        DB::table('municipal_cities')->insert([
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'BOLINEY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'BUCAY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'BUCLOC',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'DAGUIOMAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'DANGLAS',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'DOLORES',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LA PAZ',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LACUB',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LAGANGILANG',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LAGAYAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LANGIDEN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LICUAN-BAAY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'LUBA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'MALIBCONG',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'MANABO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'PEÑARRUBIA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'PIDIGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'PILAR',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'SALLAPADAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'SAN ISIDRO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'SAN JUAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'SAN QUINTIN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'TAYUM',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'TINEG',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'TUBO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'VILLAVICIOSA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'ABRA',
                'muncity' => 'BANGUED',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'CALANASAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'CONNER',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'FLORA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'KABUGAO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'LUNA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'PUDTOL',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'APAYAO',
                'muncity' => 'SANTA MARCELA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'ATOK',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'BAGUIO CITY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'BAKUN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'BOKOD',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'BUGUIAS',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'ITOGON',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'KABAYAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'KAPANGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'KIBUNGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'LA TRINIDAD',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'MANKAYAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'SABLAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'TUBA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'BENGUET',
                'muncity' => 'TUBLAY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'AGUINALDO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'ALFONSO LISTA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'ASIPULO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'BANAUE',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'HINGYON',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'HUNGDUAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'KIANGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'LAGAWE',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'LAMUT',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'MAYOYAO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'IFUGAO',
                'muncity' => 'TINOC',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'BALBALAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'TABUK CITY',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'LUBUAGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'PASIL',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'PINUKPUK',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'RIZAL',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'TANUDAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'KALINGA',
                'muncity' => 'TINGLAYAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'BARLIG',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'BAUKO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'BESAO',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'BONTOC',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'NATONIN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'PARACELIS',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'SABANGAN',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'SADANGA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'SAGADA',
                'barangay' => '',
            ],
            [
                'region' => 'CAR',
                'province' => 'MOUNTAIN PROVINCE',
                'muncity' => 'TADIAN',
                'barangay' => '',
            ]
        ]);
    }
}
