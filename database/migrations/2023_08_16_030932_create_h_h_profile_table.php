<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('h_h_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mr_id');
            $table->text('month');
            $table->text('year');
            $table->text('province_id');
            $table->text('mun_city_id');
            $table->string('hh_id', 25);
            $table->string('hh_member', 255);
            $table->string('sex', 10);
            $table->string('profile', 100);
            $table->string('pwd_type', 100);
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('h_h_profile');
    }
};
