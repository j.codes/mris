<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_a_c_meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mr_id');
            $table->text('month');
            $table->text('year');
            $table->text('province_id');
            $table->text('mun_city_id');
            $table->date('date_conducted');
            $table->text('venue');
            $table->string('agenda');
            $table->string('resolution');
            $table->date('date_sub_mom');
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_a_c_meetings');
    }
};
