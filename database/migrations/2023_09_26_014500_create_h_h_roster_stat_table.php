<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('h_h_roster_stat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mr_id');
            $table->text('month');
            $table->text('year');
            $table->text('province_id');
            $table->text('mun_city_id');
            $table->integer('c_1');
            $table->integer('c_19');
            $table->integer('c_24');
            $table->integer('total');
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('h_h_roster_stat');
    }
};
