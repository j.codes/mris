<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mr_id');
            $table->text('month');
            $table->text('year');
            $table->text('province_id');
            $table->text('mun_city_id');
            $table->text('category');
            $table->text('date_conducted');
            $table->text('title');
            $table->text('description');
            $table->text('venue');
            $table->text('objective');
            $table->integer('male');
            $table->integer('female');
            $table->integer('total');
            $table->integer('ip');
            $table->text('fund_source');
            $table->float('amount', 8, 2);
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('activities');
    }
};
