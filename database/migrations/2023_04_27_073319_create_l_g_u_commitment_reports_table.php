<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('l_g_u_commitment_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mr_id');
            $table->text('month');
            $table->text('year');
            $table->text('province_id');
            $table->text('mun_city_id');
            $table->text('barangay');
            $table->string('ssa_gap');
            $table->string('commitment');
            $table->text('timeline');
            $table->text('budget_source');
            $table->text('status');
            $table->string('remarks');
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('l_g_u_commitment_reports');
    }
};
