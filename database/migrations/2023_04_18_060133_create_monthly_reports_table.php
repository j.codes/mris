<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monthly_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('from');
            $table->date('to');
            $table->text('month');
            $table->text('year');
            $table->text('province_id')->default('Province');
            $table->text('mun_city_id')->default('Municipality');
            $table->integer('pwd_male')->default(0);
            $table->integer('pwd_female')->default(0);
            $table->integer('fff_male')->default(0);
            $table->integer('fff_female')->default(0);
            $table->integer('sp_male')->default(0);
            $table->integer('sp_female')->default(0);
            $table->boolean('isActive')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monthly_reports');
    }
};
