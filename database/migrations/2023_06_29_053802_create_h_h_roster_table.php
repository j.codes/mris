<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('h_h_roster', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->bigIncrements('id');
            $table->string('region', 100);
            $table->string('province', 100);
            $table->string('muncity', 100);
            $table->string('barangay', 100);
            $table->string('hh_id', 25);
            $table->integer('entry_id');
            $table->string('first_name', 255);
            $table->string('mid_name', 255);
            $table->string('last_name', 255);
            $table->string('ext_name', 100);
            $table->date('bday');
            $table->integer('age');
            $table->string('preg_status', 25);
            $table->string('hc_name', 255);
            $table->string('attend_school', 255);
            $table->string('dominant', 255);
            $table->string('grade_level', 100);
            $table->string('occupation', 100);
            $table->string('ip_affiliation', 50);
            $table->string('sex', 10);
            $table->string('rel_hh', 100);
            $table->string('member_status', 100);
            $table->string('disabled', 10);
            $table->string('child_bene', 100);
            $table->string('hh_grantee', 25);
            $table->integer('hh_set');
            $table->string('hh_set_grp', 10);
            $table->string('ed_attainment', 25);
            $table->string('school_name', 255);
            $table->string('registered', 10);
            $table->string('client_status', 100);
            $table->date('date_gen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('h_h_roster');
    }
};
